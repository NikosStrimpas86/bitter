# Bitter Documentation

Bitter offers the `<stdbit.h>` interface found in `C23` and later for older platforms that may not support it (yet).
It is an [STB-style](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt) header only library for C and C++.

## How to use

- Acquire the library sources.
- Add the path that leads to the `${bitter_repo_root}/include/` directory to your include path.
- In exactly one translation unit, define the macro `BITTER_IMPLEMENTATION` and include the header, as such:
```C
#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>
```
- Use the library as you wish.
- <span style="color:red">**Warning**:</span> Do not attempt to include any other headers in your project, those are internal to the library.
- <span style="color:red">**Warning**:</span> Do not use symbols, types, macros, and any other name that is not documented here in your project.
- <span style="color:blue">**Note**:</span> The library uses the prefix `bitter_` for types and functions and the prefix `BITTER_` for macros. Please don't use those in your project to avoid namespace collisions.
- <span style="color:blue">**Note**:</span> Make sure that the license of the library is compatible with your project's license before using the library.

## Types

The following types are defined by the library
```C
typedef /* language-and-platform-specific */ bitter_bool;
```
The type `bitter_bool` is defined as follows:
- In `C++` and `C23` and later, `bitter_bool` is an alias to `bool`.
- In `C99`, `C11` and `C17`, `bitter_bool` is an alias to `_Bool`.
- In `C89` or in case of an unknown language or standard, `bitter_bool` is an alias to `unsigned char`.

```C
enum bitter_endian {
    bitter_endian_little,
    bitter_endian_big,
    bitter_endian_native
};
```
The `bitter_endian` enumeration can be used to determine the endianness of the target platform. Specifically, if the 
target is little endian, the variant `bitter_endian_native` is equal to `bitter_endian_little`
