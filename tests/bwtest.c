#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_bw_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_bit_width_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::BitWidth]: bitter_bit_width_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_width_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::BitWidth]: bitter_bit_width_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_width_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::BitWidth]: bitter_bit_width_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_width_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::BitWidth]: bitter_bit_width_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_bit_width_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::BitWidth]: bitter_bit_width_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_width_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_width_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_width_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_width_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_width_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_bw_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc cases[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0x5, 3u },
        { 0xb, 4u },
        { 0x1a, 5u },
        { 0x25, 6u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_bit_width_uc(BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_width_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_bw_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us cases[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0x7, 3u },
        { 0xc, 4u },
        { 0x19, 5u },
        { 0x27, 6u },
        { 0x74, 7u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_bit_width_us(BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_width_us(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_bw_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0x0u, 0u },
        { 0x3u, 2u },
        { 0xdu, 4u },
        { 0x21u, 6u },
        { 0x33u, 6u },
        { 0x5fu, 7u },
        { 0xffu, 8u },
        { 0x1f0u, 9u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_bit_width_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_width_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_bw_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul cases[] = {
        { 0x0ul, 0u },
        { 0x5ul, 3u },
        { 0x2aul, 6u },
        { 0xbaul, 8u },
        { 0x123ul, 9u },
        { 0x563ul, 11u },
        { 0xf33ul, 12u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++ ) {
        got = bitter_bit_width_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_width_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_bw_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull cases[] = {
        { 0x0ull, 0u },
        { 0x7ull, 3u },
        { 0xfull, 4u },
        { 0x41ull, 7u },
        { 0x202ull, 10u },
        { 0xaaaull, 12u },
        { 0xffffull, 16u },
        { 0xbcdefull, 20u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_bit_width_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_width_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_bw_types();
    btest_bw_uc();
    btest_bw_us();
    btest_bw_ui();
    btest_bw_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_bw_ull();
#endif

    return 0;
}
