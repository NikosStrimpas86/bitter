#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_flo_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_first_leading_one_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingOne]: bitter_first_leading_one_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_one_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingOne]: bitter_first_leading_one_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_one_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingOne]: bitter_first_leading_one_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_one_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingOne]: bitter_first_leading_one_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_first_leading_one_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingOne]: bitter_first_leading_one_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_one_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_one_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_one_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_one_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_one_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_flo_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc inverted_tests[] = {
        { 0x5, 3u },
        { 0xa, 4u },
        { 0x1b, 5u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_first_leading_one_uc(BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_first_leading_one_uc(BITTER_CAST(unsigned char, BITTER_CAST(unsigned int, UCHAR_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    got = bitter_first_leading_one_uc(0);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_first_leading_one_uc(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - inverted_tests[idx].expected + 1u, got);
    }
}

static void btest_flo_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us inverted_tests[] = {
        { 0x6, 3u },
        { 0x9, 4u },
        { 0x17, 5u },
        { 0x2f, 6u },
        { 0x77, 7u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_first_leading_one_us(BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_first_leading_one_us(BITTER_CAST(unsigned short, BITTER_CAST(unsigned int, USHRT_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    got = bitter_first_leading_one_us(0);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_first_leading_one_us(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - inverted_tests[idx].expected + 1u, got);
    }
}

static void btest_flo_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui inverted_tests[] = {
        { 0x3u, 2u },
        { 0x6u, 3u },
        { 0xfu, 4u },
        { 0x70u, 7u },
        { 0x9au, 8u },
        { 0x743u, 11u },
        { 0x711fu, 15u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_first_leading_one_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_first_leading_one_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    got = bitter_first_leading_one_ui(0u);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_first_leading_one_ui(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - inverted_tests[idx].expected + 1u, got);
    }
}

static void btest_flo_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul inverted_tests[] = {
        { 0x5ul, 3u },
        { 0x38ul, 6u },
        { 0x76ul, 7u },
        { 0x7f3ul, 11u },
        { 0x7faaul, 15u },
        { 0xffcful, 16u },
        { 0x112121ul, 21u },
        { 0xfff1111ul, 28u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_first_leading_one_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_first_leading_one_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    got = bitter_first_leading_one_ul(0ul);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_first_leading_one_ul(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - inverted_tests[idx].expected + 1u, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_flo_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull inverted_tests[] = {
        { 0x5ull, 3u },
        { 0x39ull, 6u },
        { 0x98ull, 8u },
        { 0x6f3ull, 11u },
        { 0x7ff3ull, 15u },
        { 0x2ffffull, 18u },
        { 0x42991ull, 19u },
        { 0xfff1111ull, 28u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_first_leading_one_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_first_leading_one_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    got = bitter_first_leading_one_ull(0ull);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_first_leading_one_ull(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - inverted_tests[idx].expected + 1u, got);
    }

}

#endif

int main(void)
{
    btest_flo_types();
    btest_flo_uc();
    btest_flo_us();
    btest_flo_ui();
    btest_flo_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_flo_ull();
#endif

    return 0;
}
