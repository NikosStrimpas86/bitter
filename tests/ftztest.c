#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_ftz_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_first_trailing_zero_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingZero]: bitter_first_trailing_zero_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_zero_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingZero]: bitter_first_trailing_zero_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_zero_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingZero]: bitter_first_trailing_zero_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_zero_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingZero]: bitter_first_trailing_zero_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_first_trailing_zero_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingZero]: bitter_first_trailing_zero_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_zero_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_zero_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_zero_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_zero_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_zero_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_ftz_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc cases[] = {
        { 0x0, 1u },
        { 0x3, 3u },
        { 0x5, 2u },
        { 0x2f, 5u },
        { 0x31, 2u },
        { 0x70, 1u },
        { UCHAR_MAX, 0u }
    };

    for (idx = 1; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_first_trailing_zero_uc(BITTER_CAST(unsigned char, BITTER_CAST(unsigned int, UCHAR_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_zero_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_ftz_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us cases[] = {
        { 0x0, 1u },
        { 0x3, 3u },
        { 0x5, 2u },
        { 0x4f, 5u },
        { 0x67, 4u },
        { 0x71, 2u },
        { USHRT_MAX, 0u }
    };

    for (idx = 1; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_first_trailing_zero_us(BITTER_CAST(unsigned short, BITTER_CAST(unsigned int, USHRT_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_zero_us(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_ftz_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0x0u, 1u },
        { 0x3u, 3u },
        { 0xbu, 3u },
        { 0x5fu, 6u },
        { 0x6fu, 5u },
        { 0x7au, 1u },
        { UINT_MAX, 0u }
    };

    for (idx = 1; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_first_trailing_zero_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_zero_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_ftz_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0x0ul, 1u },
        { 0x3ul, 3u },
        { 0x1bul, 3u },
        { 0x1ful, 6u },
        { 0x3ful, 7u },
        { 0xf3ul, 3u },
        { 0x32ful, 5u },
        { 0x9fful, 10u },
        { ULONG_MAX, 0u }
    };

    for (idx = 1; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_first_trailing_zero_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_zero_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_ftz_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull cases[] = {
        { 0x0ull, 1u },
        { 0x2bull, 3u },
        { 0x77ull, 4u },
        { 0x4full, 5u },
        { 0x8ffull, 9u },
        { 0x43ffull, 11u },
        { 0x7ffffull, 20ull },
        { ULLONG_MAX, 0u }
    };

    for (idx = 1; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_first_trailing_zero_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_zero_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_ftz_types();
    btest_ftz_uc();
    btest_ftz_us();
    btest_ftz_ui();
    btest_ftz_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_ftz_ull();
#endif

    return 0;
}
