#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

void btest_cz_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_count_zeros_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::CountZeros]: bitter_count_zeros_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_zeros_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::CountZeros]: bitter_count_zeros_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_zeros_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::CountZeros]: bitter_count_zeros_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_zeros_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::CountZeros]: bitter_count_zeros_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_count_zeros_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::CountZeros]: bitter_count_zeros_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_count_zeros_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_zeros_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_zeros_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_zeros_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_count_zeros_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_cz_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc inverted_tests[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0xf, 4u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_count_zeros_uc(BITTER_CAST(unsigned char, (1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT((BITTER_UCHAR_WIDTH - 1u), got);
    }

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_count_zeros_uc(BITTER_CAST(unsigned char, (UCHAR_MAX >> BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_count_zeros_uc(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT((BITTER_UCHAR_WIDTH - inverted_tests[idx].expected), got);
    }
}

static void btest_cz_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us inverted_tests[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0x7, 3u },
        { 0xf, 4u },
        { 0x1f, 5u },
        { 0xff, 8u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_count_zeros_us(BITTER_CAST(unsigned short, (1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT((BITTER_USHRT_WIDTH - 1u), got);
    }

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_count_zeros_us(BITTER_CAST(unsigned short, (BITTER_CAST(unsigned int, USHRT_MAX) >> BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_count_zeros_us(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT((BITTER_USHRT_WIDTH - inverted_tests[idx].expected), got);
    }
}

static void btest_cz_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui inverted_tests[] = {
        { 0x0u, 0u },
        { 0x3u, 2u },
        { 0x7u, 3u },
        { 0xau, 2u },
        { 0x2fu, 5u },
        { 0x55u, 4u },
        { 0x532u, 5u },
        { 0x9ffu, 10u },
        { 0x737a, 10u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_count_zeros_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT((BITTER_UINT_WIDTH - 1u), got);
    }

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_count_zeros_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_count_zeros_ui(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT((BITTER_UINT_WIDTH - inverted_tests[idx].expected), got);
    }
}

static void btest_cz_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul inverted_tests[] = {
        { 0x0ul, 0u },
        { 0x5ul, 2u },
        { 0xful, 4u },
        { 0x33ul, 4u },
        { 0xe2ul, 4u },
        { 0xfful, 8u },
        { 0x7ffful, 15u },
        { 0xff555ul, 14u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_count_zeros_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT((BITTER_ULONG_WIDTH - 1u), got);
    }

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_count_zeros_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_count_zeros_ul(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT((BITTER_ULONG_WIDTH - inverted_tests[idx].expected), got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_cz_ull()
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull inverted_tests[] = {
        { 0x0ull, 0u },
        { 0x6ull, 2u },
        { 0xfull, 4u },
        { 0x64ull, 3u },
        { 0xfffull, 12u },
        { 0x1f14ull, 7u },
        { 0x7e11ull, 8u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_count_zeros_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT((BITTER_ULLONG_WIDTH - 1u), got);
    }

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_count_zeros_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_count_zeros_ull(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT((BITTER_ULLONG_WIDTH - inverted_tests[idx].expected), got);
    }
}

#endif

int main(void)
{
    btest_cz_types();
    btest_cz_uc();
    btest_cz_us();
    btest_cz_ui();
    btest_cz_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_cz_ull();
#endif

    return 0;
}
