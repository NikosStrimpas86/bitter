#ifdef BITTER_IMPLEMENTATION
#    error "[Bitter::Test::Extern]: Somehow, BITTER_IMPLEMENTATION is defined in a TU is was not expected to be."
#endif
#include "../tstutils.h"
#include "number.h"
#include <bitter/bitter.h>

int main(void)
{
    unsigned int number = 1u;
    unsigned int expected = bitter_count_ones_ui(number) + 1u;
    unsigned int got = get_a_number(number);
    BITTER_ASSERT_TRUE(got == expected);
    return 0;
}
