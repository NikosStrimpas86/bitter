#include "number.h"

#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>
#include "../tstutils.h"

unsigned int get_a_number(unsigned int num)
{
    unsigned int little_test = bitter_count_zeros_ui(UINT_MAX >> 1u);
    BITTER_ASSERT_TRUE(little_test == 1u);
    return bitter_count_ones_ui(num) + 1u;
}
