#ifndef BITTER_TESTS_TSTUTILS_H
#define BITTER_TESTS_TSTUTILS_H

#include <assert.h>
#include <bitter/detail/types.h>
#include <limits.h>
#include <stddef.h>

#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        define BITTER_FUNC __func__
#    else
#        define BITTER_FUNC "Unknown function"
#    endif
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 199901L
#        define BITTER_FUNC __func__
#    else
#        define BITTER_FUNC "Unknown function"
#    endif
#else
#    define BITTER_FUNC "Unknown function"
#endif

#ifdef __STDC_HOSTED__
#    include <stdio.h>
#    include <stdlib.h>

#    define BITTER_ASSERT_EQ_PARAMETRIC(exp, got, fmt, type)                                                    \
        do {                                                                                                    \
            if ((exp) != (got)) {                                                                               \
                fprintf(stderr, "%s:%d-%s: Expected: " fmt ", Got: " fmt "\n", __FILE__, __LINE__, BITTER_FUNC, \
                    BITTER_CAST(type, exp), BITTER_CAST(type, got));                                            \
                abort();                                                                                        \
            }                                                                                                   \
        } while (0)
#    define BITTER_ASSERT_TRUE(expr)                                                              \
        do {                                                                                      \
            if (!(expr)) {                                                                        \
                fprintf(stderr, "%s:%d-%s: Assertion failed\n", __FILE__, __LINE__, BITTER_FUNC); \
                abort();                                                                          \
            }                                                                                     \
        } while (0)

#    define BITTER_ASSERT_FALSE(expr)                                                             \
        do {                                                                                      \
            if (!!(expr)) {                                                                       \
                fprintf(stderr, "%s:%d-%s: Assertion failed\n", __FILE__, __LINE__, BITTER_FUNC); \
                abort();                                                                          \
            }                                                                                     \
        } while (0)

#    define BITTER_ASSERT_EQ_UCHAR(exp, got) BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%u", unsigned int)
#    define BITTER_ASSERT_EQ_USHRT(exp, got) BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%u", unsigned int)
#    define BITTER_ASSERT_EQ_UINT(exp, got)  BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%u", unsigned int)
#    define BITTER_ASSERT_EQ_ULONG(exp, got) BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%lu", unsigned long)
#    if BITTER_HAS_UNSIGNED_LONG_LONG
#        define BITTER_ASSERT_EQ_ULLONG(exp, got) BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%llu", unsigned long long)
#    endif
#    define BITTER_ASSERT_EQ_SIZE(exp, got) BITTER_ASSERT_EQ_PARAMETRIC(exp, got, "%zu", size_t)

#else
#    define BITTER_ASSERT_TRUE(expr)         assert(!!(expr))
#    define BITTER_ASSERT_FALSE(expr)        assert(!(expr))
#    define BITTER_ASSERT_EQ_UCHAR(exp, got) assert((exp) == (got))
#    define BITTER_ASSERT_EQ_USHRT(exp, got) assert((exp) == (got))
#    define BITTER_ASSERT_EQ_UINT(exp, got)  assert((exp) == (got))
#    define BITTER_ASSERT_EQ_ULONG(exp, got) assert((exp) == (got))
#    if BITTER_HAS_UNSIGNED_LONG_LONG
#        define BITTER_ASSERT_EQ_ULLONG(exp, got) assert((exp) == (got))
#    endif
#    define BITTER_ASSERT_EQ_SIZE(exp, got) assert((exp) == (got))
#endif

#ifdef __cplusplus
#    define BITTER_TEST_BOOL  bool
#    define BITTER_TEST_TRUE  true
#    define BITTER_TEST_FALSE false
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 202311L
#        define BITTER_TEST_BOOL  bool
#        define BITTER_TEST_TRUE  true
#        define BITTER_TEST_FALSE false
#    elif (__STDC_VERSION__ >= 199901L) && (__STDC_VERSION__ < 202311L)
#        include <stdbool.h>
#        define BITTER_TEST_BOOL  bool
#        define BITTER_TEST_TRUE  true
#        define BITTER_TEST_FALSE false
#    else
#        define BITTER_TEST_BOOL  int
#        define BITTER_TEST_TRUE  1
#        define BITTER_TEST_FALSE 0
#    endif
#else
#    define BITTER_TEST_BOOL  int
#    define BITTER_TEST_TRUE  1
#    define BITTER_TEST_FALSE 0
#endif
typedef BITTER_TEST_BOOL bitter_test_bool;

#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        define BITTER_TEST_HAS_STATIC_ASSERT
#    endif
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 202311L
#        define BITTER_TEST_HAS_STATIC_ASSERT
#    elif __STDC_VERSION__ >= 201112L
#        define BITTER_TEST_HAS_STATIC_ASSERT
#    endif
#endif

#if defined(__STDC_VERSION__) && !defined(__cplusplus)
#    if __STDC_VERSION__ >= 201112L
#        define BITTER_TEST_HAS_GENERIC_SELECTION
#    endif
#endif

struct bitter_arg_exp_uc {
    unsigned char arg;
    unsigned int expected;
};

struct bitter_arg_exp_us {
    unsigned short arg;
    unsigned int expected;
};

struct bitter_arg_exp_ui {
    unsigned int arg;
    unsigned int expected;
};

struct bitter_arg_exp_ul {
    unsigned long arg;
    unsigned int expected;
};

#if BITTER_HAS_UNSIGNED_LONG_LONG
struct bitter_arg_exp_ull {
    unsigned long long arg;
    unsigned int expected;
};
#endif

struct bitter_pair_uc {
    unsigned char arg;
    unsigned char expected;
};

struct bitter_pair_us {
    unsigned short arg;
    unsigned short expected;
};

struct bitter_pair_ui {
    unsigned int arg;
    unsigned int expected;
};

struct bitter_pair_ul {
    unsigned long arg;
    unsigned long expected;
};

#if BITTER_HAS_UNSIGNED_LONG_LONG

struct bitter_pair_ull {
    unsigned long long arg;
    unsigned long long expected;
};

#endif

#endif /* BITTER_TESTS_TSTUTILS_H */
