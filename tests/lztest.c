#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_lz_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_leading_zeros_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::LeadingZeros]: bitter_leading_zeros_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_leading_zeros_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::LeadingZeros]: bitter_leading_zeros_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_leading_zeros_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::LeadingZeros]: bitter_leading_zeros_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_leading_zeros_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::LeadingZeros]: bitter_leading_zeros_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_leading_zeros_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::LeadingZeros]: bitter_leading_zeros_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_leading_zeros_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_leading_zeros_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_leading_zeros_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_leading_zeros_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_leading_zeros_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_lz_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc inverted_tests[] = {
        { 0x0, 0u },
        { 0x5, 3u },
        { 0xb, 4u },
        { 0x2b, 6u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_leading_zeros_uc(BITTER_CAST(unsigned char, BITTER_CAST(unsigned int, UCHAR_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(idx, got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_leading_zeros_uc(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_UCHAR_WIDTH - inverted_tests[idx].expected, got);
    }
}

static void btest_lz_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us inverted_tests[] = {
        { 0x0, 0u },
        { 0x5, 3u },
        { 0xa, 4u },
        { 0x1b, 5u },
        { 0x2f, 6u },
        { 0x71, 7u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_leading_zeros_us(BITTER_CAST(unsigned short, BITTER_CAST(unsigned int, USHRT_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(idx, got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_leading_zeros_us(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_USHRT_WIDTH - inverted_tests[idx].expected, got);
    }
}

static void btest_lz_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui inverted_tests[] = {
        { 0x0u, 0u },
        { 0x3u, 2u },
        { 0x6u, 3u },
        { 0xfu, 4u },
        { 0x36u, 6u },
        { 0x99u, 8u },
        { 0x7f3u, 11u },
        { 0x7fffu, 15u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_leading_zeros_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(idx, got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_leading_zeros_ui(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_UINT_WIDTH - inverted_tests[idx].expected, got);
    }
}

static void btest_lz_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul inverted_tests[] = {
        { 0x0ul, 0u },
        { 0x6ul, 3u },
        { 0x36ul, 6u },
        { 0x99ul, 8u },
        { 0x7f3ul, 11u },
        { 0x7ffful, 15u },
        { 0xfffful, 16u },
        { 0x212121ul, 22u },
        { 0xfff1111ul, 28u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_leading_zeros_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(idx, got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_leading_zeros_ul(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_ULONG_WIDTH - inverted_tests[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_lz_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull inverted_tests[] = {
        { 0x0ull, 0u },
        { 0x7ull, 3u },
        { 0x36ull, 6u },
        { 0x99ull, 8u },
        { 0x7f3ull, 11u },
        { 0x7fffull, 15u },
        { 0x1ffffull, 17u },
        { 0x72121ull, 19u },
        { 0xfff1111ull, 28u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_leading_zeros_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(idx, got);
    }

    for (idx = 0; idx < (sizeof(inverted_tests) / sizeof(inverted_tests[0])); idx++) {
        got = bitter_leading_zeros_ull(inverted_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_ULLONG_WIDTH - inverted_tests[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_lz_types();
    btest_lz_uc();
    btest_lz_us();
    btest_lz_ui();
    btest_lz_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_lz_ull();
#endif

    return 0;
}
