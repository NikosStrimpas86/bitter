#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_to_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_trailing_ones_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::TrailingOnes]: bitter_trailing_ones_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_ones_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::TrailingOnes]: bitter_trailing_ones_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_ones_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::TrailingOnes]: bitter_trailing_ones_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_ones_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::TrailingOnes]: bitter_trailing_ones_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_trailing_ones_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::TrailingOnes]: bitter_trailing_ones_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_ones_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_ones_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_ones_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_ones_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_ones_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_to_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc cases[] = {
        { 0x0, 0u },
        { 0x2, 0u },
        { 0x7, 3u },
        { 0x8, 0u },
        { 0x9, 1u },
        { 0x30, 0u },
        { 0x1f, 5u },
        { 0x23, 2u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_trailing_ones_uc(BITTER_CAST(unsigned char, BITTER_CAST(unsigned int, UCHAR_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_ones_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_to_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us cases[] = {
        { 0x0, 0u },
        { 0x2, 0u },
        { 0x7, 3u },
        { 0x8, 0u },
        { 0x9, 1u },
        { 0x30, 0u },
        { 0x1f, 5u },
        { 0x23, 2u },
        { 0x55, 1u },
        { 0x7f, 7u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_trailing_ones_us(BITTER_CAST(unsigned short, BITTER_CAST(unsigned int, USHRT_MAX) >> (BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_ones_us(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_to_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0x0u, 0u },
        { 0x2u, 0u },
        { 0x7u, 3u },
        { 0x10u, 0u },
        { 0x31u, 1u },
        { 0x2fu, 4u },
        { 0x35u, 1u },
        { 0x5fu, 5u },
        { 0x7fu, 7u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_trailing_ones_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_ones_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_to_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul cases[] = {
        { 0x0ul, 0u },
        { 0x2ul, 0u },
        { 0x7ul, 3u },
        { 0x10ul, 0u },
        { 0x33ul, 2u },
        { 0x3ful, 6u },
        { 0x36ul, 0u },
        { 0x5ful, 5u },
        { 0x7ful, 7u },
        { 0xfful, 8u },
        { 0x5fful, 9u },
        { 0x5f7ul, 3u },
        { 0x635ul, 1u },
        { 0x7fful, 11u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_trailing_ones_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_ones_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_to_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull cases[] = {
        { 0x0ull, 0u },
        { 0x2ull, 0u },
        { 0x7ull, 3u },
        { 0x10ull, 0u },
        { 0x33ull, 2u },
        { 0x3full, 6u },
        { 0x52ull, 0u },
        { 0x6full, 4u },
        { 0x7full, 7u },
        { 0xffull, 8u },
        { 0x1ffull, 9u },
        { 0x5f0ull, 0u },
        { 0x635ull, 1u },
        { 0xfffull, 12u },
        { 0x6483full, 6u },
        { 0x5fffful, 17u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_trailing_ones_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_ones_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_to_types();
    btest_to_uc();
    btest_to_us();
    btest_to_ui();
    btest_to_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_to_ull();
#endif

    return 0;
}
