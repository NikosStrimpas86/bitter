#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_flz_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_first_leading_zero_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingZero]: bitter_first_leading_zero_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_zero_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingZero]: bitter_first_leading_zero_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_zero_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingZero]: bitter_first_leading_zero_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_leading_zero_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingZero]: bitter_first_leading_zero_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_first_leading_zero_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::FirstLeadingZero]: bitter_first_leading_zero_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_zero_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_zero_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_zero_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_zero_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_first_leading_zero_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_flz_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc reverse_tests[] = {
        { 0x1, 1u },
        { 0x5, 3u },
        { 0xa, 4u },
        { 0x3b, 6u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_first_leading_zero_uc(BITTER_CAST(unsigned char, ~(1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    got = bitter_first_leading_zero_uc(UCHAR_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(reverse_tests) / sizeof(reverse_tests[0])); idx++) {
        got = bitter_first_leading_zero_uc(BITTER_CAST(unsigned char, (~BITTER_CAST(unsigned int, reverse_tests[idx].arg)) & BITTER_CAST(unsigned int, UCHAR_MAX)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - reverse_tests[idx].expected + 1u, got);
    }
}

static void btest_flz_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us reverse_tests[] = {
        { 0x1, 1u },
        { 0x7, 3u },
        { 0x23, 6u },
        { 0x41, 7u },
        { 0x7f, 7u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_first_leading_zero_us(BITTER_CAST(unsigned short, ~(1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    got = bitter_first_leading_zero_us(USHRT_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(reverse_tests) / sizeof(reverse_tests[0])); idx++) {
        got = bitter_first_leading_zero_us(BITTER_CAST(unsigned short, (~BITTER_CAST(unsigned int, reverse_tests[idx].arg)) & BITTER_CAST(unsigned int, USHRT_MAX)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - reverse_tests[idx].expected + 1u, got);
    }
}

static void btest_flz_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui reverse_tests[] = {
        { 0x3u, 2u },
        { 0x8u, 4u },
        { 0x63u, 7u },
        { 0xf6u, 8u },
        { 0x6f9u, 11u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_first_leading_zero_ui(~(1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    got = bitter_first_leading_zero_ui(UINT_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(reverse_tests) / sizeof(reverse_tests[0])); idx++) {
        got = bitter_first_leading_zero_ui(~reverse_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - reverse_tests[idx].expected + 1u, got);
    }
}

static void btest_flz_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui reverse_tests[] = {
        { 0x3ul, 2u },
        { 0x13ul, 5u },
        { 0xfful, 8u },
        { 0x3737ul, 14u },
        { 0x7ffful, 15u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_first_leading_zero_ul(~(1ul << BITTER_CAST(unsigned long, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    got = bitter_first_leading_zero_ul(ULONG_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(reverse_tests) / sizeof(reverse_tests[0])); idx++) {
        got = bitter_first_leading_zero_ul(~reverse_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - reverse_tests[idx].expected + 1u, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_flz_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull reverse_tests[] = {
        { 0x1ull, 1u },
        { 0x39ull, 6u },
        { 0xffull, 8u },
        { 0x7121ull, 15u },
        { 0xfffffull, 20u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_first_leading_zero_ull(~(1ull << BITTER_CAST(unsigned long long, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - BITTER_CAST(unsigned int, idx), got);
    }

    got = bitter_first_leading_zero_ull(ULLONG_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);

    for (idx = 0; idx < (sizeof(reverse_tests) / sizeof(reverse_tests[0])); idx++) {
        got = bitter_first_leading_zero_ull(~reverse_tests[idx].arg);
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - reverse_tests[idx].expected + 1u, got);
    }
}

#endif

int main(void)
{
    btest_flz_types();
    btest_flz_uc();
    btest_flz_us();
    btest_flz_ui();
    btest_flz_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_flz_ull();
#endif

    return 0;
}
