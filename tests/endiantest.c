#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

#ifndef BITTER_ENDIAN_LITTLE
#    error "[Bitter::Test::Endian]: BITTER_ENDIAN_LITTLE is not defined!"
#endif

#ifndef BITTER_ENDIAN_BIG
#    error "[Bitter::Test::Endian]: BITTER_ENDIAN_BIG is not defined!"
#endif

#ifndef BITTER_ENDIAN_NATIVE
#    error "[Bitter::Test::Endian]: BITTER_ENDIAN_NATIVE is not defined!"
#endif

void btest_endian_values(void)
{
#ifdef BITTER_TEST_HAS_STATIC_ASSERT
    static_assert(bitter_endian_little == BITTER_ENDIAN_LITTLE,
        "[Bitter::Test::Endian]: Variant bitter_endian_little of bitter_endian "
        "does not have the same value as BITTER_ENDIAN_LITTLE.");
    static_assert(bitter_endian_big == BITTER_ENDIAN_BIG,
        "[Bitter::Test::Endian]: Variant bitter_endian_little of bitter_endian does "
        "not have the same value as BITTER_ENDIAN_BIG");
    static_assert(bitter_endian_native == BITTER_ENDIAN_NATIVE,
        "[Bitter::Test::Endian]: Variant bitter_endian_native of bitter_endian "
        "does not have the same value as BITTER_ENDIAN_NATIVE");
#else
    BITTER_ASSERT_TRUE(bitter_endian_little == BITTER_ENDIAN_LITTLE);
    BITTER_ASSERT_TRUE(bitter_endian_big == BITTER_ENDIAN_BIG);
    BITTER_ASSERT_TRUE(bitter_endian_native == BITTER_ENDIAN_NATIVE);
#endif
    (void)0;
}

void btest_endian(void)
{
#if BITTER_ENDIAN_NATIVE == BITTER_ENDIAN_LITTLE
#    if CHAR_BIT == 8
    unsigned int try = 0x22ffu;
    unsigned char part = *(unsigned char*)&try;
    BITTER_ASSERT_EQ_UINT((try & UCHAR_MAX), BITTER_CAST(unsigned int, part));
#    else
#        error "TODO"
#    endif
#elif BITTER_ENDIAN_NATIVE == BITTER_ENDIAN_BIG
#    error "TODO"
#else
#    error "TODO"
#endif
}

int main(void)
{
    btest_endian_values();
    btest_endian();
    return 0;
}
