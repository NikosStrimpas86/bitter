#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_bf_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_bit_floor_uc(dummy_uc), unsigned char: true, default: false),
        "[Bitter::Test::BitFloor]: bitter_bit_floor_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_floor_us(dummy_us), unsigned short: true, default: false),
        "[Bitter::Test::BitFloor]: bitter_bit_floor_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_floor_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::BitFloor]: bitter_bit_floor_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_floor_ul(dummy_ul), unsigned long: true, default: false),
        "[Bitter::Test::BitFloor]: bitter_bit_floor_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_bit_floor_ull(dummy_ull), unsigned long long: true, default: false),
        "[Bitter::Test::BitFloor]: bitter_bit_floor_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_floor_uc(dummy_uc)) == sizeof(unsigned char));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_floor_us(dummy_us)) == sizeof(unsigned short));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_floor_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_floor_ul(dummy_ul)) == sizeof(unsigned long));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_floor_ull(dummy_ull)) == sizeof(unsigned long long));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_bf_uc(void)
{
    size_t idx;
    unsigned char got;
    unsigned char identity;
    struct bitter_pair_uc cases[] = {
        { 0x3, 0x2 },
        { 0x7, 0x4 },
        { 0xa, 0x8 },
        { 0x18, 0x10 },
        { 0x55, 0x40 },
        { 0x71, 0x40 },
        { 0x0, 0x0 }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        identity = BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx));
        got = bitter_bit_floor_uc(identity);
        BITTER_ASSERT_EQ_UCHAR(identity, got);
    }

    got = bitter_bit_floor_uc(UCHAR_MAX);
    BITTER_ASSERT_EQ_UCHAR(BITTER_CAST(unsigned char, 1u << (BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - 1u)), got);

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_floor_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UCHAR(cases[idx].expected, got);
    }
}

static void btest_bf_us(void)
{
    size_t idx;
    unsigned short identity;
    unsigned short got;
    struct bitter_pair_us cases[] = {
        { 0x3, 0x2 },
        { 0x5, 0x4 },
        { 0x9, 0x8 },
        { 0x12, 0x10 },
        { 0x37, 0x20 },
        { 0x68, 0x40 },
        { 0x0, 0x0 }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        identity = BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx));
        got = bitter_bit_floor_us(identity);
        BITTER_ASSERT_EQ_USHRT(identity, got);
    }

    got = bitter_bit_floor_us(USHRT_MAX);
    BITTER_ASSERT_EQ_USHRT(BITTER_CAST(unsigned short, 1u << (BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - 1u)), got);

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_floor_us(cases[idx].arg);
        BITTER_ASSERT_EQ_USHRT(cases[idx].expected, got);
    }
}

static void btest_bf_ui(void)
{
    size_t idx;
    unsigned int identity;
    unsigned int got;
    struct bitter_pair_ui cases[] = {
        { 0x3u, 0x2u },
        { 0x7u, 0x4u },
        { 0x16u, 0x10u },
        { 0x2au, 0x20u },
        { 0x55u, 0x40u },
        { 0x81u, 0x80u },
        { 0x0u, 0x0u}
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        identity = 1u << BITTER_CAST(unsigned int, idx);
        got = bitter_bit_floor_ui(identity);
        BITTER_ASSERT_EQ_UINT(identity, got);
    }

    got = bitter_bit_floor_ui(UINT_MAX);
    BITTER_ASSERT_EQ_UINT(1u << (BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - 1u), got);

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_floor_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_bf_ul(void)
{
    size_t idx;
    unsigned long identity;
    unsigned long got;
    struct bitter_pair_ul cases[] = {
        { 0x3ul, 0x2ul },
        { 0x5ul, 0x4ul },
        { 0x9ul, 0x8ul },
        { 0x3bul, 0x20ul },
        { 0x45bul, 0x400ul },
        { 0xbcbul, 0x800ul },
        { 0x4ffful, 0x4000ul },
        { 0x0ul, 0x0ul }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        identity = 1ul << BITTER_CAST(unsigned long, idx);
        got = bitter_bit_floor_ul(identity);
        BITTER_ASSERT_EQ_ULONG(identity, got);
    }

    got = bitter_bit_floor_ul(ULONG_MAX);
    BITTER_ASSERT_EQ_UINT(1ul << (BITTER_CAST(unsigned long, BITTER_ULONG_WIDTH) - 1ul), got);

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_floor_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_bf_ull(void)
{
    size_t idx;
    unsigned long long identity;
    unsigned long long got;
    struct bitter_pair_ull cases[] = {
        { 0x3ull, 0x2ull },
        { 0x13ull, 0x10ull },
        { 0x2cull, 0x20ull },
        { 0x9full, 0x80ull },
        { 0x632ull, 0x400ull },
        { 0x5f99ull, 0x4000ull },
        { 0x0ull, 0x0ull }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        identity = 1ull << BITTER_CAST(unsigned long long, idx);
        got = bitter_bit_floor_ull(identity);
        BITTER_ASSERT_EQ_ULLONG(identity, got);
    }

    got = bitter_bit_floor_ull(ULLONG_MAX);
    BITTER_ASSERT_EQ_ULLONG(1ull << (BITTER_CAST(unsigned long long, idx) - 1ull), got);

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_floor_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_ULLONG(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_bf_types();
    btest_bf_uc();
    btest_bf_us();
    btest_bf_ui();
    btest_bf_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_bf_ull();
#endif

    return 0;
}
