#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_tz_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_trailing_zeros_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::TrailingZeros]: bitter_trailing_zeros_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_zeros_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::TrailingZeros]: bitter_trailing_zeros_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_zeros_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::TrailingZeros]: bitter_trailing_zeros_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_trailing_zeros_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::TrailingZeros]: bitter_trailing_zeros_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_trailing_zeros_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::TrailingZeros]: bitter_trailing_zeros_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_zeros_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_zeros_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_zeros_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_zeros_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_trailing_zeros_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_tz_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc cases[] = {
        { 0x0, BITTER_UCHAR_WIDTH },
        { 0x6, 1u },
        { 0xc, 2u },
        { 0x10, 4u },
        { 0x20, 5u },
        { 0x22, 1u },
        { 0x33, 0u },
        { 0x40, 6u },
        { 0x78, 3u },
        { UCHAR_MAX, 0u }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_trailing_zeros_uc(BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_trailing_zeros_uc(BITTER_CAST(unsigned char, BITTER_CAST(unsigned int, UCHAR_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(0u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_zeros_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_tz_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us cases[] = {
        { 0x0, BITTER_USHRT_WIDTH },
        { 0x6, 1u },
        { 0xc, 2u },
        { 0x10, 4u },
        { 0x12, 1u },
        { 0x20, 5u },
        { 0x22, 1u },
        { 0x34, 2u },
        { 0x40, 6u },
        { 0x78, 3u },
        { USHRT_MAX, 0u }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_trailing_zeros_us(BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_trailing_zeros_us(BITTER_CAST(unsigned short, BITTER_CAST(unsigned int, UINT_MAX) >> BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(0u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_zeros_us(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_tz_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0u, BITTER_UINT_WIDTH },
        { 0x6u, 1u },
        { 0xcu, 2u },
        { 0x12u, 1u },
        { 0x20u, 5u },
        { 0x34u, 2u },
        { 0x40u, 6u },
        { 0x78u, 3u },
        { 0x100u, 8u },
        { 0x140u, 6u },
        { 0x600u, 9u },
        { UINT_MAX, 0u }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_trailing_zeros_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_trailing_zeros_ui(UINT_MAX >> BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(0u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_zeros_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_tz_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul cases[] = {
        { 0ul, BITTER_ULONG_WIDTH },
        { 0x6ul, 1u },
        { 0xcul, 2u },
        { 0x12ul, 1u },
        { 0x20ul, 5u },
        { 0x34ul, 2u },
        { 0x40ul, 6u },
        { 0x78ul, 3u },
        { 0x100ul, 8u },
        { 0x140ul, 6u },
        { 0x600ul, 9u },
        { 0xff00ul, 8u },
        { 0xff90ul, 4u },
        { 0x8920ul, 5u },
        { ULONG_MAX, 0u }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_trailing_zeros_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_trailing_zeros_ul(ULONG_MAX >> BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(0u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_zeros_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

void btest_tz_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull cases[] = {
        { 0x0ull, BITTER_ULLONG_WIDTH },
        { 0x6ull, 1u },
        { 0xcull, 2u },
        { 0x12ull, 1u },
        { 0x20ull, 5u },
        { 0x34ull, 2u },
        { 0x40ull, 6u },
        { 0x78ull, 3u },
        { 0x100ull, 8u },
        { 0x140ull, 6u },
        { 0x600ull, 9u },
        { 0xff00ull, 8u },
        { 0xff90ull, 4u },
        { 0x8920ull, 5u },
        { 0x55550ull, 4u },
        { 0x120000ull, 17u },
        { 0x780058ull, 3u },
        { 0x900000ull, 20u },
        { ULLONG_MAX, 0u }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_trailing_zeros_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx), got);
    }

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_trailing_zeros_ull(ULLONG_MAX >> BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(0u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_trailing_zeros_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_tz_types();
    btest_tz_uc();
    btest_tz_us();
    btest_tz_ui();
    btest_tz_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_tz_ull();
#endif
    return 0;
}
