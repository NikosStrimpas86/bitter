#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

void btest_co_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_count_ones_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::CountOnes]: bitter_count_ones_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_ones_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::CountOnes]: bitter_count_ones_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_ones_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::CountOnes]: bitter_count_ones_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_count_ones_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::CountOnes]: bitter_count_ones_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_count_ones_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::CountOnes]: bitter_count_ones_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_count_ones_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_ones_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_ones_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_count_ones_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_count_ones_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_co_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc inputs_and_results[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0x7, 3u },
        { 0xf, 4u },
        { 0x7f, 7u },
        { UCHAR_MAX, BITTER_UCHAR_WIDTH }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_count_ones_uc(BITTER_CAST(unsigned char, (1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(1u, got);
    }

    for (idx = 0; idx < (sizeof(inputs_and_results) / sizeof(inputs_and_results[0])); idx++) {
        got = bitter_count_ones_uc(inputs_and_results[idx].arg);
        BITTER_ASSERT_EQ_UINT(inputs_and_results[idx].expected, got);
    }

}

static void btest_co_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us inputs_and_results[] = {
        { 0x0, 0u },
        { 0x3, 2u },
        { 0x7 , 3u },
        { 0xf, 4u },
        { 0x2f, 5u },
        { 0xff, 8u },
        { 0x9ff, 10u },
        { USHRT_MAX, BITTER_USHRT_WIDTH }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_count_ones_us(BITTER_CAST(unsigned short, (1u << BITTER_CAST(unsigned int, idx))));
        BITTER_ASSERT_EQ_UINT(1u, got);
    }

    for (idx = 0; idx < (sizeof(inputs_and_results) / sizeof(inputs_and_results[0])); idx++) {
        got = bitter_count_ones_us(inputs_and_results[idx].arg);
        BITTER_ASSERT_EQ_USHRT(inputs_and_results[idx].expected, got);
    }
}

static void btest_co_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui inputs_and_results[] = {
        { 0x0u, 0u },
        { 0x3u, 2u },
        { 0x7u, 3u },
        { 0x9u, 2u },
        { 0xfu, 4u },
        { 0x52u, 3u },
        { 0x77u, 6u },
        { 0x7f1u, 8u },
        { 0x999u, 6u },
        { UINT_MAX, BITTER_UINT_WIDTH }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_count_ones_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(1u, got);
    }

    for (idx = 0; idx < (sizeof(inputs_and_results) / sizeof(inputs_and_results[0])); idx++) {
        got = bitter_count_ones_ui(inputs_and_results[idx].arg);
        BITTER_ASSERT_EQ_UINT(inputs_and_results[idx].expected, got);
    }
}

static void btest_co_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul inputs_and_results[] = {
        { 0x0ul, 0u },
        { 0x3ul, 2u },
        { 0x6ul, 2u },
        { 0xf1ul, 5u },
        { 0x1011ul, 3u },
        { 0xfffful, 16u },
        { 0x1f431ul, 9u },
        { 0xff555ul, 14u },
        { ULONG_MAX, BITTER_ULONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_count_ones_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(1u, got);
    }

    for (idx = 0; idx < (sizeof(inputs_and_results) / sizeof(inputs_and_results[0])); idx++) {
        got = bitter_count_ones_ul(inputs_and_results[idx].arg);
        BITTER_ASSERT_EQ_UINT(inputs_and_results[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_co_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull inputs_and_results[] = {
        { 0x0ull, 0u },
        { 0x3ull, 2u },
        { 0x11ull, 2u },
        { 0x65ull, 4u },
        { 0x77ull, 6u },
        { 0x5151ull, 6u },
        { 0x1cc11ull, 7u },
        { 0xffffffffull, 32u },
        { ULLONG_MAX, BITTER_ULLONG_WIDTH }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_count_ones_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(1u, got);
    }

    for (idx = 0; idx < (sizeof(inputs_and_results) / sizeof(inputs_and_results[0])); idx++) {
        got = bitter_count_ones_ull(inputs_and_results[idx].arg);
        BITTER_ASSERT_EQ_UINT(inputs_and_results[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_co_types();
    btest_co_uc();
    btest_co_us();
    btest_co_ui();
    btest_co_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_co_ull();
#endif
    return 0;
}
