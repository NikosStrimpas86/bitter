#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

void btest_hsb_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_has_single_bit_uc(dummy_uc), bitter_bool: true, default: false),
        "[Bitter::Test::HasSingleBit]: bitter_has_single_bit_uc return type is not correct (expected: bitter_bool)");
    static_assert(_Generic(bitter_has_single_bit_us(dummy_us), bitter_bool: true, default: false),
        "[Bitter::Test::HasSingleBit]: bitter_has_single_bit_us return type is not correct (expected: bitter_bool)");
    static_assert(_Generic(bitter_has_single_bit_ui(dummy_ui), bitter_bool: true, default: false),
        "[Bitter::Test::HasSingleBit]: bitter_has_single_bit_ui return type is not correct (expected: bitter_bool)");
    static_assert(_Generic(bitter_has_single_bit_ul(dummy_ul), bitter_bool: true, default: false),
        "[Bitter::Test::HasSingleBit]: bitter_has_single_bit_ul return type is not correct (expected: bitter_bool)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_has_single_bit_ull(dummy_ull), bitter_bool: true, default: false),
        "[Bitter::Test::HasSingleBit]: bitter_has_single_bit_ull return type is not correct (expected: bitter_bool)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_has_single_bit_uc(dummy_uc)) == sizeof(bitter_bool));
    BITTER_ASSERT_TRUE(sizeof(bitter_has_single_bit_us(dummy_us)) == sizeof(bitter_bool));
    BITTER_ASSERT_TRUE(sizeof(bitter_has_single_bit_ui(dummy_ui)) == sizeof(bitter_bool));
    BITTER_ASSERT_TRUE(sizeof(bitter_has_single_bit_ul(dummy_ul)) == sizeof(bitter_bool));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_has_single_bit_ull(dummy_ull)) == sizeof(bitter_bool));
#    endif
#endif
    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

void btest_hsb_uc(void)
{
    size_t idx;
    bitter_bool got;
    unsigned char other_numbers[] = {
        0x0,
        0x3,
        0x5,
        0x6,
        0x7,
        0x9,
        0xa,
        0xb,
        0xc,
        0xd,
        0xe,
        0xf,
        0x11,
        0x7f,
        UCHAR_MAX
    };


    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_has_single_bit_uc(BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_TRUE(got);
    }

    for (idx = 0; idx < (sizeof(other_numbers) / sizeof(unsigned char)); idx++) {
        got = bitter_has_single_bit_uc(other_numbers[idx]);
        BITTER_ASSERT_FALSE(got);
    }

}

void btest_hsb_us(void)
{
    size_t idx;
    bitter_bool got;
    unsigned short other_numbers[] = {
        0x0,
        0x3,
        0x7,
        0x88,
        0x314,
        0x003,
        0x555,
        USHRT_MAX
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_has_single_bit_us(BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int,idx)));
        BITTER_ASSERT_TRUE(got);
    }

    for (idx = 0; idx < (sizeof(other_numbers) / sizeof(unsigned short)); idx++) {
        got = bitter_has_single_bit_us(other_numbers[idx]);
        BITTER_ASSERT_FALSE(got);
    }
}

void btest_hsb_ui(void)
{
    size_t idx;
    bitter_bool got;
    unsigned int other_numbers[] = {
        0x0u,
        0x3u,
        0x88u,
        0x89u,
        0x898u,
        0x1010u,
        0x7fffu,
        UINT_MAX
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_has_single_bit_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_TRUE(got);
    }

    for (idx = 0; idx < (sizeof(other_numbers) / sizeof(unsigned int)); idx++) {
        got = bitter_has_single_bit_ui(other_numbers[idx]);
        BITTER_ASSERT_FALSE(got);
    }
}

void btest_hsb_ul(void)
{
    size_t idx;
    bitter_bool got;
    unsigned long other_numbers[] = {
        0x0ul,
        0x3ul,
        0x76ul,
        0x85ul,
        0x898ul,
        0x1010ul,
        0x7ffful,
        0x74747ul,
        0x555698ul,
        0x12345678ul,
        ULONG_MAX
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_has_single_bit_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_TRUE(got);
    }

    for (idx = 0; idx < (sizeof(other_numbers) / sizeof(unsigned long)); idx++) {
        got = bitter_has_single_bit_ul(other_numbers[idx]);
        BITTER_ASSERT_FALSE(got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

void btest_hsb_ull(void)
{
    size_t idx;
    bitter_bool got;
    unsigned long long other_numbers[] = {
        0x0ull,
        0x3ull,
        0x76ull,
        0x85ull,
        0x898ull,
        0x1010ull,
        0x7fffull,
        0x74147ull,
        0x500698ull,
        0x12345678ull,
        ULLONG_MAX
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_has_single_bit_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_TRUE(got);
    }

    for (idx = 0; idx < (sizeof(other_numbers) / sizeof(unsigned long long)); idx++) {
        got = bitter_has_single_bit_ull(other_numbers[idx]);
        BITTER_ASSERT_FALSE(got);
    }
}

#endif

int main(void)
{
    btest_hsb_types();
    btest_hsb_uc();
    btest_hsb_us();
    btest_hsb_ui();
    btest_hsb_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_hsb_ull();
#endif
    return 0;
}
