#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_bc_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_bit_ceil_uc(dummy_uc), unsigned char: true, default: false),
        "[Bitter::Test::BitCeil]: bitter_bit_ceil_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_ceil_us(dummy_us), unsigned short: true, default: false),
        "[Bitter::Test::BitCeil]: bitter_bit_ceil_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_ceil_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::BitCeil]: bitter_bit_ceil_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_bit_ceil_ul(dummy_ul), unsigned long: true, default: false),
        "[Bitter::Test::BitCeil]: bitter_bit_ceil_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_bit_ceil_ull(dummy_ull), unsigned long long: true, default: false),
        "[Bitter::Test::BitCeil]: bitter_bit_ceil_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_ceil_uc(dummy_uc)) == sizeof(unsigned char));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_ceil_us(dummy_us)) == sizeof(unsigned short));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_ceil_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_ceil_ul(dummy_ul)) == sizeof(unsigned long));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_bit_ceil_ull(dummy_ull)) == sizeof(unsigned long long));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_bc_uc(void)
{
    size_t idx;
    unsigned char identity;
    unsigned char got;
    struct bitter_pair_uc cases[] = {
        { 0x3, 0x4 },
        { 0x5, 0x8 },
        { 0x7, 0x8 },
        { 0x9, 0x10 },
        { 0x15, 0x20 },
        { 0x22, 0x40 },
        { 0x3f, 0x40 },
        { 0x0, 0x1 }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        identity = BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx));
        got = bitter_bit_ceil_uc(identity);
        BITTER_ASSERT_EQ_UCHAR(identity, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_ceil_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UCHAR(cases[idx].expected, got);
    }

#ifndef BITTER_BIT_CEIL_UB
    got = bitter_bit_ceil_uc(UCHAR_MAX);
    BITTER_ASSERT_EQ_UCHAR(0, got);
    got = bitter_bit_ceil_uc(UCHAR_MAX - 0x3);
    BITTER_ASSERT_EQ_UCHAR(0, got);
    got = bitter_bit_ceil_uc(UCHAR_MAX - 0xf);
    BITTER_ASSERT_EQ_UCHAR(0, got);
#endif
}

static void btest_bc_us(void)
{
    size_t idx;
    unsigned short identity;
    unsigned short got;
    struct bitter_pair_us cases[] = {
        { 0x3, 0x4 },
        { 0x5, 0x8 },
        { 0x6, 0x8 },
        { 0xb, 0x10 },
        { 0x12, 0x20 },
        { 0x25, 0x40 },
        { 0x3c, 0x40 },
        { 0x0, 0x1 }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        identity = BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx));
        got = bitter_bit_ceil_us(identity);
        BITTER_ASSERT_EQ_USHRT(identity, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_ceil_us(cases[idx].arg);
        BITTER_ASSERT_EQ_USHRT(cases[idx].expected, got);
    }

#ifndef BITTER_BIT_CEIL_UB
    got = bitter_bit_ceil_us(USHRT_MAX);
    BITTER_ASSERT_EQ_USHRT(0, got);
    got = bitter_bit_ceil_us(USHRT_MAX - 0x3);
    BITTER_ASSERT_EQ_USHRT(0, got);
    got = bitter_bit_ceil_us(USHRT_MAX - 0xf);
    BITTER_ASSERT_EQ_USHRT(0, got);
#endif
}

static void btest_bc_ui(void)
{
    size_t idx;
    unsigned int identity;
    unsigned int got;
    struct bitter_pair_ui cases[] = {
        { 0x3u, 0x4u },
        { 0x5u, 0x8u },
        { 0x7u, 0x8u },
        { 0xcu, 0x10u },
        { 0x13u, 0x20u },
        { 0x27u, 0x40u },
        { 0x3au, 0x40u },
        { 0x55u, 0x80u },
        { 0x7au, 0x80u },
        { 0x0u, 0x1u }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        identity = 1u << BITTER_CAST(unsigned int, idx);
        got = bitter_bit_ceil_ui(identity);
        BITTER_ASSERT_EQ_UINT(identity, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_ceil_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }

#ifndef BITTER_BIT_CEIL_UB
    got = bitter_bit_ceil_ui(UINT_MAX);
    BITTER_ASSERT_EQ_UINT(0u, got);
    got = bitter_bit_ceil_ui(UINT_MAX - 0x6u);
    BITTER_ASSERT_EQ_UINT(0u, got);
    got = bitter_bit_ceil_ui(UINT_MAX - 0xeu);
    BITTER_ASSERT_EQ_UINT(0u, got);
#endif
}

static void btest_bc_ul(void)
{
    size_t idx;
    unsigned long identity;
    unsigned long got;
    struct bitter_pair_ul cases[] = {
        { 0x3ul, 0x4ul },
        { 0x6ul, 0x8ul },
        { 0xcul, 0x10ul },
        { 0x1aul, 0x20ul },
        { 0x27ul, 0x40ul },
        { 0x3aul, 0x40ul },
        { 0x5dul, 0x80ul },
        { 0x134ul, 0x200ul },
        { 0x301ul, 0x400ul },
        { 0x43dul, 0x800ul },
        { 0xb77ul, 0x1000ul },
        { 0x0ul, 0x1ul }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        identity = 1ul << BITTER_CAST(unsigned long, idx);
        got = bitter_bit_ceil_ul(identity);
        BITTER_ASSERT_EQ_ULONG(identity, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_ceil_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_ULONG(cases[idx].expected, got);
    }

#ifndef BITTER_BIT_CEIL_UB
    got = bitter_bit_ceil_ul(ULONG_MAX);
    BITTER_ASSERT_EQ_ULONG(0ul, got);
    got = bitter_bit_ceil_ul(ULONG_MAX - 0x15ul);
    BITTER_ASSERT_EQ_ULONG(0ul, got);
    got = bitter_bit_ceil_ul(ULONG_MAX - 0xfful);
    BITTER_ASSERT_EQ_ULONG(0ul, got);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_bc_ull(void)
{
    size_t idx;
    unsigned long long identity;
    unsigned long long got;
    struct bitter_pair_ull cases[] = {
        { 0x3ull, 0x4ull },
        { 0xcull, 0x10ull },
        { 0x1aull, 0x20ull },
        { 0x5dull, 0x80ull },
        { 0x134ull, 0x200ull },
        { 0x42dull, 0x800ull },
        { 0x8bcull, 0x1000ull },
        { 0x189aull, 0x2000ull },
        { 0x2500ull, 0x4000ull },
        { 0x400eull, 0x8000ull },
        { 0x0ull, 0x1ull }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        identity = 1ull << BITTER_CAST(unsigned long long, idx);
        got = bitter_bit_ceil_ull(identity);
        BITTER_ASSERT_EQ_ULLONG(identity, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_bit_ceil_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_ULLONG(cases[idx].expected, got);
    }

#ifndef BITTER_BIT_CEIL_UB
    got = bitter_bit_ceil_ull(ULLONG_MAX);
    BITTER_ASSERT_EQ_ULLONG(0ull, got);
    got = bitter_bit_ceil_ull(ULLONG_MAX - 0x35ull);
    BITTER_ASSERT_EQ_ULLONG(0ull, got);
    got = bitter_bit_ceil_ull(ULLONG_MAX - 0xfdull);
    BITTER_ASSERT_EQ_ULLONG(0ull, got);
#endif
}

#endif

int main(void)
{
    btest_bc_types();
    btest_bc_uc();
    btest_bc_us();
    btest_bc_ui();
    btest_bc_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_bc_ull();
#endif

    return 0;
}
