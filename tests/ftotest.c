#define BITTER_IMPLEMENTATION
#include <bitter/bitter.h>

#include "tstutils.h"

static void btest_fto_types(void)
{
    unsigned char const dummy_uc = 0;
    unsigned short const dummy_us = 0;
    unsigned int const dummy_ui = 0u;
    unsigned long const dummy_ul = 0ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    unsigned long long const dummy_ull = 0ull;
#endif

#if defined(BITTER_TEST_HAS_STATIC_ASSERT) && defined(BITTER_TEST_HAS_GENERIC_SELECTION)
    static_assert(_Generic(bitter_first_trailing_one_uc(dummy_uc), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingOne]: bitter_first_trailing_one_uc return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_one_us(dummy_us), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingOne]: bitter_first_trailing_one_us return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_one_ui(dummy_ui), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingOne]: bitter_first_trailing_one_ui return type is not correct (expected: unsigned int)");
    static_assert(_Generic(bitter_first_trailing_one_ul(dummy_ul), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingOne]: bitter_first_trailing_one_ul return type is not correct (expected: unsigned int)");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    static_assert(_Generic(bitter_first_trailing_one_ull(dummy_ull), unsigned int: true, default: false),
        "[Bitter::Test::FirstTrailingOne]: bitter_first_trailing_one_ull return type is not correct (expected: unsigned int)");
#    endif
#else
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_one_uc(dummy_uc)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_one_us(dummy_us)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_one_ui(dummy_ui)) == sizeof(unsigned int));
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_one_ul(dummy_ul)) == sizeof(unsigned int));
#    if BITTER_HAS_UNSIGNED_LONG_LONG
    BITTER_ASSERT_TRUE(sizeof(bitter_first_trailing_one_ull(dummy_ull)) == sizeof(unsigned int));
#    endif
#endif

    (void)dummy_uc;
    (void)dummy_us;
    (void)dummy_ui;
    (void)dummy_ul;
#if BITTER_HAS_UNSIGNED_LONG_LONG
    (void)dummy_ull;
#endif
}

static void btest_fto_uc(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_uc cases[] = {
        { 0x0, 0u },
        { 0x3, 1u },
        { 0x6, 2u },
        { 0x7, 1u },
        { 0x10, 5u },
        { 0x5c, 3u },
        { 0x60, 6u },
        { 0x7f, 1u },
        { UCHAR_MAX, 1u }
    };

    for (idx = 0; idx < BITTER_UCHAR_WIDTH; idx++) {
        got = bitter_first_trailing_one_uc(BITTER_CAST(unsigned char, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_one_uc(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_fto_us(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_us cases[] = {
        { 0x0, 0u },
        { 0x3, 1u },
        { 0x6, 2u },
        { 0x7, 1u },
        { 0x20, 6u },
        { 0x5a, 2u },
        { 0x6e, 2u },
        { 0x7f, 1u },
        { USHRT_MAX, 1u }
    };

    for (idx = 0; idx < BITTER_USHRT_WIDTH; idx++) {
        got = bitter_first_trailing_one_us(BITTER_CAST(unsigned short, 1u << BITTER_CAST(unsigned int, idx)));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_one_us(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_fto_ui(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ui cases[] = {
        { 0x0u, 0u },
        { 0x3u, 1u },
        { 0x6u, 2u },
        { 0x7u, 1u },
        { 0x18u, 4u },
        { 0x50u, 5u },
        { 0x7fu, 1u },
        { 0x80u, 8u },
        { 0x120u, 6u },
        { UINT_MAX, 1u }
    };

    for (idx = 0; idx < BITTER_UINT_WIDTH; idx++) {
        got = bitter_first_trailing_one_ui(1u << BITTER_CAST(unsigned int, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_one_ui(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

static void btest_fto_ul(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ul cases[] = {
        { 0x0ul, 0u },
        { 0x3ul, 1u },
        { 0x6ul, 2u },
        { 0x50ul, 5u },
        { 0x7ful, 1u },
        { 0x80ul, 8u },
        { 0x120ul, 6u },
        { 0x500ul, 9u },
        { 0x7fful, 1u },
        { ULONG_MAX, 1u }
    };

    for (idx = 0; idx < BITTER_ULONG_WIDTH; idx++) {
        got = bitter_first_trailing_one_ul(1ul << BITTER_CAST(unsigned long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_one_ul(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

static void btest_fto_ull(void)
{
    size_t idx;
    unsigned int got;
    struct bitter_arg_exp_ull cases[] = {
        { 0x0ull, 0u },
        { 0x3ull, 1u },
        { 0x6ull, 2u },
        { 0x50ull, 5u },
        { 0x7full, 1u },
        { 0x80ull, 8u },
        { 0x160ull, 6u },
        { 0x600ull, 10u },
        { 0x8000ull, 16u },
        { 0xf000ull, 13ul },
        { ULLONG_MAX, 1u }
    };

    for (idx = 0; idx < BITTER_ULLONG_WIDTH; idx++) {
        got = bitter_first_trailing_one_ull(1ull << BITTER_CAST(unsigned long long, idx));
        BITTER_ASSERT_EQ_UINT(BITTER_CAST(unsigned int, idx) + 1u, got);
    }

    for (idx = 0; idx < (sizeof(cases) / sizeof(cases[0])); idx++) {
        got = bitter_first_trailing_one_ull(cases[idx].arg);
        BITTER_ASSERT_EQ_UINT(cases[idx].expected, got);
    }
}

#endif

int main(void)
{
    btest_fto_types();
    btest_fto_uc();
    btest_fto_us();
    btest_fto_ui();
    btest_fto_ul();
#if BITTER_HAS_UNSIGNED_LONG_LONG
    btest_fto_ull();
#endif

    return 0;
}
