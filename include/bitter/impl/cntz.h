/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_CNTZ_H
#define BITTER_IMPL_CNTZ_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_zeros_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_zeros)
    return __builtin_stdc_count_zeros(value);
#else
    return bitter_count_ones_uc((~BITTER_CAST(unsigned int, value)) & UCHAR_MAX);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_zeros_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_zeros)
    return __builtin_stdc_count_zeros(value);
#else
    return bitter_count_ones_us((~BITTER_CAST(unsigned int, value)) & USHRT_MAX);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_zeros_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_zeros)
    return __builtin_stdc_count_zeros(value);
#else
    return bitter_count_ones_ui(~value);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_zeros_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_zeros)
    return __builtin_stdc_count_zeros(value);
#else
    return bitter_count_ones_ul(~value);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_count_zeros_ull(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_count_zeros)
    return __builtin_stdc_count_zeros(value);
#    else
    return bitter_count_ones_ull(~value);
#    endif
}

#endif

#endif /* BITTER_IMPL_CNTZ_H */
