/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_FLZ_H
#define BITTER_IMPL_FLZ_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_zero_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_zero)
    return __builtin_stdc_first_leading_zero(value);
#else
    return value == UCHAR_MAX ? 0u : (bitter_leading_ones_uc(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_zero_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_zero)
    return __builtin_stdc_first_leading_zero(value);
#else
    return value == USHRT_MAX ? 0u : (bitter_leading_ones_us(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_zero_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_zero)
    return __builtin_stdc_first_leading_zero(value);
#else
    return value == UINT_MAX ? 0u : (bitter_leading_ones_ui(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_zero_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_zero)
    return __builtin_stdc_first_leading_zero(value);
#else
    return value == ULONG_MAX ? 0u : (bitter_leading_ones_ul(value) + 1u);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_zero_ull(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_zero)
    return __builtin_stdc_first_leading_zero(value);
#    else
    return value == ULLONG_MAX ? 0u : (bitter_leading_ones_ull(value) + 1u);
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_FLZ_H */
