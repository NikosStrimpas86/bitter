/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_BITCEIL_H
#define BITTER_IMPL_BITCEIL_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned char bitter_bit_ceil_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
#    ifdef BITTER_BIT_CEIL_UB
    return stdc_bit_ceil_uc(value);
#    else
    return BITTER_UNLIKELY(
               BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - 1u)))
               ? BITTER_CAST(unsigned char, 0)
               : stdc_bit_ceil_uc(value);
#    endif
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_ceil)
#    ifdef BITTER_BIT_CEIL_UB
    return __builtin_stdc_bit_ceil(value);
#    else
    BITTER_UNLIKELY(BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - 1u)))
    ? BITTER_CAST(unsigned char, 0) : __builtin_stdc_bit_ceil(value);
#    endif
#else
#    ifndef BITTER_BIT_CEIL_UB
    if (BITTER_UNLIKELY(
            BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - 1u))))
        return 0;
#    endif
    return (value <= 1) ? BITTER_CAST(unsigned char, 1)
                        : BITTER_CAST(unsigned char,
                            1u << (BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - bitter_leading_zeros_uc(value - 1)));
#endif
}

BITTER_UNSEQ_DECL unsigned short bitter_bit_ceil_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
#    ifdef BITTER_BIT_CEIL_UB
    return stdc_bit_ceil_us(value);
#    else
    return BITTER_UNLIKELY(
               BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - 1u)))
               ? BITTER_CAST(unsigned short, 0)
               : stdc_bit_ceil_us(value);
#    endif
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_ceil)
#    ifdef BITTER_BIT_CEIL_UB
    return __builtin_stdc_bit_ceil(value);
#    else
    return BITTER_UNLIKELY(
               BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - 1u)))
               ? BITTER_CAST(unsigned short, 0)
               : __builtin_stdc_bit_ceil(value);
#    endif
#else
#    ifndef BITTER_BIT_CEIL_UB
    if (BITTER_UNLIKELY(
            BITTER_CAST(unsigned int, value) > (1u << (BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - 1u))))
        return 0;
#    endif
    return (value <= 1) ? BITTER_CAST(unsigned short, 1)
                        : BITTER_CAST(unsigned short,
                            1u << (BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - bitter_leading_zeros_us(value - 1)));
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_bit_ceil_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
#    ifdef BITTER_BIT_CEIL_UB
    return stdc_bit_ceil_ui(value);
#    else
    return BITTER_UNLIKELY(value > (1u << (BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - 1u)))
               ? 0u
               : stdc_bit_ceil_ui(value);
#    endif
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_ceil)
#    ifdef BITTER_BIT_CEIL_UB
    return __builtin_stdc_bit_ceil(value);
#    else
    return BITTER_UNLIKELY(value > (1u << (BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - 1u)))
               ? 0u
               : __builtin_stdc_bit_ceil(value);
#    endif
#else
#    ifndef BITTER_BIT_CEIL_UB
    if (BITTER_UNLIKELY(value > (1u << (BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - 1u))))
        return 0u;
#    endif
    return (value <= 1u) ? 1u
                         : (1u << (BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - bitter_leading_zeros_ui(value - 1u)));
#endif
}

BITTER_UNSEQ_DECL unsigned long bitter_bit_ceil_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
#    ifdef BITTER_BIT_CEIL_UB
    return stdc_bit_ceil_ul(value);
#    else
    return BITTER_UNLIKELY(value > (1ul << (BITTER_CAST(unsigned long, BITTER_ULONG_WIDTH) - 1ul)))
               ? 0ul
               : stdc_bit_ceil_ul(value);
#    endif
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_ceil)
#    ifdef BITTER_BIT_CEIL_UB
    return __builtin_stdc_bit_ceil(value);
#    else
    return BITTER_UNLIKELY(value > (1ul << (BITTER_CAST(unsigned long, BITTER_ULONG_WIDTH) - 1ul)))
               ? 0ul
               : __builtin_stdc_bit_ceil(value);
#    endif
#else
#    ifndef BITTER_BIT_CEIL_UB
    if (BITTER_UNLIKELY(value > (1ul << (BITTER_CAST(unsigned long, BITTER_ULONG_WIDTH) - 1ul))))
        return 0ul;
#    endif
    return (value <= 1ul)
               ? 1ul
               : (1ul << (BITTER_CAST(unsigned long, BITTER_ULONG_WIDTH) - bitter_leading_zeros_ul(value - 1ul)));
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned long long bitter_bit_ceil_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
#        ifdef BITTER_BIT_CEIL_UB
    return stdc_bit_ceil_ull(value);
#        else
    return BITTER_UNLIKELY(value > (1ull << (BITTER_CAST(unsigned long long, BITTER_ULLONG_WIDTH) - 1ull)))
               ? 0ull
               : stdc_bit_ceil_ull(value);
#        endif
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_ceil)
#        ifdef BITTER_BIT_CEIL_UB
    return __builtin_stdc_bit_ceil(value);
#        else
    return BITTER_UNLIKELY(value > (1ull << (BITTER_CAST(unsigned long long, BITTER_ULLONG_WIDTH) - 1ull)))
               ? 0ull
               : __builtin_stdc_bit_ceil(value);
#        endif
#    else
#        ifndef BITTER_BIT_CEIL_UB
    if (BITTER_UNLIKELY(value > (1ull << (BITTER_CAST(unsigned long long, BITTER_ULLONG_WIDTH) - 1ull))))
        return 0ull;
#        endif
    return (value <= 1ull) ? 1ull
                           : (1ull << (BITTER_CAST(unsigned long long, BITTER_ULLONG_WIDTH)
                                       - bitter_leading_zeros_ull(value - 1ull)));
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_BITCEIL_H */
