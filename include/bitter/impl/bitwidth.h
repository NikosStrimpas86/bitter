/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_BITWIDTH_H
#define BITTER_IMPL_BITWIDTH_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_width_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_width)
    return __builtin_stdc_bit_width(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return BITTER_CAST(unsigned int, value) == 0u ? 0u : (1u + _bsr(BITTER_CAST(unsigned int, value)));
#else
    return BITTER_UCHAR_WIDTH - bitter_leading_zeros_uc(value);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_width_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_width)
    return __builtin_stdc_bit_width(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return BITTER_CAST(unsigned int, value) == 0u ? 0u : (1u + _bsr(BITTER_CAST(unsigned int, value)));
#else
    return BITTER_USHRT_WIDTH - bitter_leading_zeros_us(value);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_width_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_width)
    return __builtin_stdc_bit_width(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0u ? 0u : (1u + _bsr(value));
#else
    return BITTER_UINT_WIDTH - bitter_leading_zeros_ui(value);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_width_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_width)
    return __builtin_stdc_bit_width(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0ul ? 0u : (1u + _bsr(BITTER_CAST(unsigned int, value)));
#else
    return BITTER_ULONG_WIDTH - bitter_leading_zeros_ul(value);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_width_ull(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_width)
    return __builtin_stdc_bit_width(value);
#    elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    unsigned int high;

    if (value == 0ull)
        return 0u;

    high = BITTER_CAST(unsigned int, value >> BITTER_UINT_WIDTH);
    if (high != 0u)
        return BITTER_UINT_WIDTH + 1u + _bsr(high);

    /* clang-format off */
    return 1u + _bsr(BITTER_CAST(unsigned int, value & UINT_MAX));
    /* clang-format on */
#    else
    return BITTER_ULLONG_WIDTH - bitter_leading_zeros_ull(value);
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_BITWIDTH_H */
