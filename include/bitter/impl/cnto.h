/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_CNTO_H
#define BITTER_IMPL_CNTO_H

#include <bitter/detail/prelude.h>
#include <bitter/kernels/kcnto.h>

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_ones_uc(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED) && defined(__AVX__)
    return __popcnt16(BITTER_CAST(unsigned short, value));
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountOneBits(BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_ones)
    return __builtin_stdc_count_ones(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcountg)
    return __builtin_popcountg(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcount) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__KEFIRCC__)
    return __builtin_popcount(BITTER_CAST(unsigned int, value));
#else
    unsigned int res;
    BITTER_CNTO_UC_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_ones_us(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED) && defined(__AVX__)
    return __popcnt16(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountOneBits(BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_ones)
    return __builtin_stdc_count_ones(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcountg)
    return __builtin_popcountg(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcount) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__KEFIRCC__)
    return __builtin_popcount(BITTER_CAST(unsigned int, value));
#else
    unsigned int res;
    BITTER_CNTO_US_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_ones_ui(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED) && defined(__AVX__)
    return __popcnt(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountOneBits(BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_ones)
    return __builtin_stdc_count_ones(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcountg)
    return __builtin_popcountg(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcount) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__KEFIRCC__)
    return __builtin_popcount(value);
#else
    unsigned int res;
    BITTER_CNTO_UI_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_count_ones_ul(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED) && defined(__AVX__)
#    if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) == sizeof(unsigned int), "[Bitter::CountOnes]: Argument value would be truncated!");
#    endif
    return __popcnt(BITTER_CAST(unsigned int, value));
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountOneBits(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_count_ones)
    return __builtin_stdc_count_ones(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcountg)
    return __builtin_popcountg(value);
#elif BITTER_HAS_BUILTIN(__builtin_popcountl) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__KEFIRCC__)
    return __builtin_popcountl(value);
#else
    unsigned int res;
    BITTER_CNTO_UL_IMPL(res, value);
    return res;
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_count_ones_ull(value);
#    elif defined(_MSC_VER)                                                                             \
        && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
            && !defined(_M_ARM64EC))                                                                    \
        && !defined(_M_CEE) && !defined(_MANAGED) && defined(__AVX__)
#        ifdef _M_IX86
#            if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) > sizeof(unsigned int),
        "[Bitter::CountOnes]: Undefined behaviour due to shift with exponent larger than the width of unsigned "
        "long long would occur!");
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) <= 2 * sizeof(unsigned int),
        "[Bitter::CountOnes]: Two phase calculation of count ones would not work!");
#            endif
    /* clang-format off */
    return __popcnt(BITTER_CAST(unsigned int, value & BITTER_CAST(unsigned long long, UINT_MAX)))
           + __popcnt(BITTER_CAST(unsigned int, value >> BITTER_UINT_WIDTH));
    /* clang-format on */
#        else
    return BITTER_CAST(unsigned int, __popcnt64(value));
#        endif
#    elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
        && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountOneBits64(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_count_ones)
    return __builtin_stdc_count_ones(value);
#    elif BITTER_HAS_BUILTIN(__builtin_popcountg)
    return __builtin_popcountg(value);
#    elif BITTER_HAS_BUILTIN(__builtin_popcountll) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__KEFIRCC__)
    return __builtin_popcountll(value);
#    else
    unsigned int res;
    BITTER_CNTO_ULL_IMPL(res, value);
    return res;
#    endif
}

#endif

#endif /* BITTER_IMPL_CNTO_H */
