/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_TZ_H
#define BITTER_IMPL_TZ_H

#include <bitter/detail/prelude.h>
#include <bitter/kernels/ktz.h>

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_trailing_zeros_uc(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned int) > sizeof(unsigned char), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#        endif
    return _tzcnt_u32(~BITTER_CAST(unsigned int, UCHAR_MAX) | BITTER_CAST(unsigned int, value));
#    else
    unsigned long result;
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) > sizeof(unsigned char), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#        endif
    (void)_BitScanForward(&result, ~BITTER_CAST(unsigned long, UCHAR_MAX) | BITTER_CAST(unsigned long, value));
    return BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
#    if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) > sizeof(unsigned char), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#    endif
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm64-intrinsics?view=msvc-170 */
    return _CountTrailingZeros(~BITTER_CAST(unsigned long, UCHAR_MAX) | BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_trailing_zeros)
    return __builtin_stdc_trailing_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_ctzg)
    return __builtin_ctzg(value, BITTER_UCHAR_WIDTH);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return _bsf(~BITTER_CAST(unsigned int, UCHAR_MAX) | BITTER_CAST(unsigned int, value));
#else
    unsigned int res;
    BITTER_TZ_UC_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_trailing_zeros_us(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned int) > sizeof(unsigned short), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#        endif
    return _tzcnt_u32(~BITTER_CAST(unsigned int, USHRT_MAX) | BITTER_CAST(unsigned int, value));
#    else
    unsigned long result;
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) > sizeof(unsigned short), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#        endif
    (void)_BitScanForward(&result, ~BITTER_CAST(unsigned long, USHRT_MAX) | BITTER_CAST(unsigned long, value));
    return BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
#    if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) > sizeof(unsigned short), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#    endif
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm64-intrinsics?view=msvc-170 */
    return _CountTrailingZeros(~BITTER_CAST(unsigned long, USHRT_MAX) | BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_trailing_zeros)
    return __builtin_stdc_trailing_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_ctzg)
    return __builtin_ctzg(value, BITTER_USHRT_WIDTH);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return _bsf(~BITTER_CAST(unsigned int, USHRT_MAX) | BITTER_CAST(unsigned int, value));
#else
    unsigned int res;
    BITTER_TZ_US_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_trailing_zeros_ui(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
    return _tzcnt_u32(value);
#    else
    unsigned long result;
    if (!_BitScanForward(&result, BITTER_CAST(unsigned long, value)))
        return BITTER_UINT_WIDTH;
    return BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
#    if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) >= sizeof(unsigned int), "[Bitter::TrailingZeros]: The trick of masks would not work!");
#    endif
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm64-intrinsics?view=msvc-170 */
    return _CountTrailingZeros(~BITTER_CAST(unsigned long, UINT_MAX) | BITTER_CAST(unsigned long, value));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_trailing_zeros)
    return __builtin_stdc_trailing_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_ctzg)
    return __builtin_ctzg(value, BITTER_UINT_WIDTH);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0u ? BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) : BITTER_CAST(unsigned int, _bsf(value));
#else
    unsigned int res;
    BITTER_TZ_UI_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_trailing_zeros_ul(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long) == sizeof(unsigned int),
        "[Bitter::TrailingZeros]: Function argument could be truncated!");
#        endif
    return _tzcnt_u32(BITTER_CAST(unsigned int, value));
#    else
    unsigned long result;
    if (!_BitScanForward(&result, value))
        return BITTER_ULONG_WIDTH;
    return BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm64-intrinsics?view=msvc-170 */
    return _CountTrailingZeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_trailing_zeros)
    return __builtin_stdc_trailing_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_ctzg)
    return __builtin_ctzg(value, BITTER_ULONG_WIDTH);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0ul ? BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH)
                        : BITTER_CAST(unsigned int, _bsf(BITTER_CAST(unsigned int, value)));
#else
    unsigned int res;
    BITTER_TZ_UL_IMPL(res, value);
    return res;
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_traling_zeros_ull(value);
#    elif defined(_MSC_VER)                                                                             \
        && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
            && !defined(_M_ARM64EC))                                                                    \
        && !defined(_M_CEE) && !defined(_MANAGED)
#        if defined(__AVX2__)
#            ifdef _M_IX86
    unsigned int high;
    unsigned int low;

#                if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) > sizeof(unsigned int),
        "[Bitter::TrailingZeros]: Undefined behaviour due to shift with exponent larger than the width of unsigned "
        "long long would occur!");
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) <= 2 * sizeof(unsigned int),
        "[Bitter::TrailingZeros]: Two phase calculation of leading zeros would not work!");
#                endif

    /* clang-format off */
    low = BITTER_CAST(unsigned int, value & BITTER_CAST(unsigned long long, UINT_MAX));
    /* clang-format on */
    if (low == 0u) {
        high = BITTER_CAST(unsigned int, value >> BITTER_UINT_WIDTH);
        return BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) + _tzcnt_u32(high)
               - (high == 0u ? 0u : BITTER_CAST(unsigned int, 2 * BITTER_UINT_WIDTH - BITTER_ULLONG_WIDTH));
    } else
        return _tzcnt_u32(low);
#            else
    return BITTER_CAST(unsigned int, _tzcnt_u64(value));
#            endif
#        else
#            ifdef _M_IX86
    unsigned long high;
    unsigned long low;
    unsigned long result;

#                if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) > sizeof(unsigned long),
        "[Bitter::TrailingZeros]: Undefined behaviour due to shift with exponent larger than the width of unsigned "
        "long long would occur!");
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) <= 2 * sizeof(unsigned long),
        "[Bitter::TrailingZeros]: Two phase calculation of leading zeros would not work!");
#                endif

    /* clang-format off */
    low = BITTER_CAST(unsigned long, value & BITTER_CAST(unsigned long long, ULONG_MAX));
    /* clang-format on */
    if (_BitScanForward(&result, low))
        return BITTER_CAST(unsigned int, result);

    high = BITTER_CAST(unsigned long, value >> BITTER_ULONG_WIDTH);
    if (!_BitScanForward(&result, high))
        return BITTER_ULLONG_WIDTH;

    return BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) + BITTER_CAST(unsigned int, result);
#            else
    unsigned long result;
    if (!_BitScanForward64(&result, value))
        return BITTER_ULLONG_WIDTH;
    return result;
#            endif
#        endif
#    elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
        && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm64-intrinsics?view=msvc-170 */
    return _CountTrailingZeros64(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_trailing_zeros)
    return __builtin_stdc_trailing_zeros(value);
#    elif BITTER_HAS_BUILTIN(__builtin_ctzg)
    return __builtin_ctzg(value, BITTER_ULLONG_WIDTH);
#    elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    unsigned int low;
    unsigned int high;

    if (value == 0ull)
        return BITTER_ULLONG_WIDTH;

    /* clang-format off */
    low = BITTER_CAST(unsigned int, value & BITTER_CAST(unsigned long long, UINT_MAX));
    /* clang-format on */
    if (low != 0u)
        return _bsf(low);

    high = BITTER_CAST(unsigned int, value >> BITTER_UINT_WIDTH);
    return BITTER_UINT_WIDTH + _bsf(high);
#    else
    unsigned int res;
    BITTER_TZ_ULL_IMPL(res, value);
    return res;
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_TZ_H */
