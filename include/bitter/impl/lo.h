/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_LO_H
#define BITTER_IMPL_LO_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_ones_uc(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountLeadingOnes(~BITTER_CAST(unsigned long, UCHAR_MAX) | BITTER_CAST(unsigned long, value))
           - (BITTER_ULONG_WIDTH - BITTER_UCHAR_WIDTH);
#else
    return bitter_leading_zeros_uc((~BITTER_CAST(unsigned int, value)) & UCHAR_MAX);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_ones_us(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountLeadingOnes(~BITTER_CAST(unsigned long, USHRT_MAX) | BITTER_CAST(unsigned long, value))
           - (BITTER_ULONG_WIDTH - BITTER_USHRT_WIDTH);
#else
    return bitter_leading_zeros_us((~BITTER_CAST(unsigned int, value)) & USHRT_MAX);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_ones_ui(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountLeadingOnes(~BITTER_CAST(unsigned long, UINT_MAX) | BITTER_CAST(unsigned long, value))
           - (BITTER_ULONG_WIDTH - BITTER_UINT_WIDTH);
#else
    return bitter_leading_zeros_ui(~value);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_ones_ul(value);
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountLeadingOnes(value);
#else
    return bitter_leading_zeros_ul(~value);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_ones_ull(value);
#    elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
        && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    return _CountLeadingOnes64(value);
#    else
    return bitter_leading_zeros_ull(~value);
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_LO_H */
