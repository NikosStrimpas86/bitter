/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_BITFLOOR_H
#define BITTER_IMPL_BITFLOOR_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned char bitter_bit_floor_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_floor_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_floor)
    return __builtin_stdc_bit_floor(value);
#else
    if (value == 0)
        return 0;
    return (unsigned char)(1u << (BITTER_UCHAR_WIDTH - 1u - bitter_leading_zeros_uc(value)));
#endif
}

BITTER_UNSEQ_DECL unsigned short bitter_bit_floor_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_floor_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_floor)
    return __builtin_stdc_bit_floor(value);
#else
    if (value == 0)
        return 0;
    return (unsigned short)(1u << (BITTER_USHRT_WIDTH - 1u - bitter_leading_zeros_us(value)));
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_bit_floor_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_floor_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_floor)
    return __builtin_stdc_bit_floor(value);
#else
    if (value == 0u)
        return 0u;
    return 1u << (BITTER_UINT_WIDTH - 1u - bitter_leading_zeros_ui(value));
#endif
}

BITTER_UNSEQ_DECL unsigned long bitter_bit_floor_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_floor_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_floor)
    return __builtin_stdc_bit_floor(value);
#else
    if (value == 0ul)
        return 0ul;
    return 1ul << (BITTER_ULONG_WIDTH - 1ul - bitter_leading_zeros_ul(value));
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned long long bitter_bit_floor_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_bit_floor_ull(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_bit_floor)
    return __builtin_stdc_bit_floor(value);
#    else
    if (value == 0ull)
        return 0ull;
    return 1ull << (BITTER_ULLONG_WIDTH - 1ull - bitter_leading_zeros_ull(value));
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_BITFLOOR_H */
