/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_ENDIAN_H
#define BITTER_IMPL_ENDIAN_H

#include <bitter/detail/util.h>

#include <limits.h>

/* clang-format off */
/* TODO: Find out which platforms support those */
#ifdef __GLIBC__
#    include <endian.h>
#elif defined(__APPLE__)
#    include <machine/endian.h>
#elif defined(__FreeBSD__) || BITTER_HAS_INCLUDE(<sys/endian.h>)
#    include <sys/endian.h>
#endif
/* clang-format on */

#define BITTER_ENDIAN_LITTLE_IMPL    1
#define BITTER_ENDIAN_BIG_IMPL       2
#define BITTER_ENDIAN_PDP_IMPL       4
#define BITTER_ENDIAN_BI_IMPL        8
#define BITTER_ENDIAN_UNDEFINED_IMPL 16

#if defined(__STDC_ENDIAN_LITTLE__) && defined(__STDC_ENDIAN_BIG__) && defined(__STDC_ENDIAN_NATIVE__)
#    if __STDC_ENDIAN_NATIVE__ == __STDC_ENDIAN_LITTLE__
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    elif __STDC_ENDIAN_NATIVE__ == __STDC_ENDIAN_BIG__
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(__ORDER_LITTLE_ENDIAN__) && defined(__ORDER_BIG_ENDIAN__) && defined(__BYTE_ORDER__)
/*
 * Found in the following compilers:
 * GCC: https://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html
 * Clang: (GCC compatibility) -> (ICX, OpenXL, C++ Builder, FujitsuClang, Apple Clang, CrayClang, ArmClang too probably)
 * Intel ICC: (Trust me bro)
 * TCC: https://repo.or.cz/tinycc.git/blob/HEAD:/include/tccdefs.h#l59
 * CompCert: (Trust me bro)
 * Kefir: https://sr.ht/~jprotopopov/kefir/#language-extensions
 */
#    if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    elif defined(__ORDER_PDP_ENDIAN__) && (__BYTE_ORDER__ == __ORDER_PDP_ENDIAN__)
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_PDP_IMPL
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(__big_endian__) && !defined(__little_endian__)
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif !defined(__big_endian__) && defined(__little_endian__)
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__BIG_ENDIAN__) && !defined(__LITTLE_ENDIAN__)
/* TODO: Verify (IBM I think) */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif (!defined(__BIG_ENDIAN__) && defined(__LITTLE_ENDIAN__)) && !defined(__IAR_SYSTEMS_ICC__)
/* TODO: Verify (IBM I think) */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(BYTE_ORDER) && defined(LITTLE_ENDIAN) && defined(BIG_ENDIAN)
#    if BYTE_ORDER == LITTLE_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    elif BYTE_ORDER == BIG_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    elif defined(PDP_ENDIAN)
#        if BYTE_ORDER == PDP_ENDIAN
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_PDP_IMPL
#        else
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#        endif
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(__BYTE_ORDER) && defined(__LITTLE_ENDIAN) && defined(__BIG_ENDIAN)
#    if __BYTE_ORDER == __LITTLE_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    elif __BYTE_ORDER == __BIG_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    elif defined(__PDP_ENDIAN)
#        if __BYTE_ORDER == __PDP_ENDIAN
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_PDP_IMPL
#        else
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#        endif
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(_BYTE_ORDER) && defined(_LITTLE_ENDIAN) && defined(_BIG_ENDIAN)
#    if _BYTE_ORDER == _LITTLE_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    elif _BYTE_ORDER == _BIG_ENDIAN
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    elif defined(_PDP_ENDIAN)
#        if _BYTE_ORDER == _PDP_ENDIAN
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_PDP_IMPL
#        else
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#        endif
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(__COMPCERT__)
#    if defined(__ppc__) || defined(__PPC__) || defined(__ARMEB__)
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    elif defined(__i386__) || defined(__x86_64__) || defined(__ARMEL__) || defined(__aarch64__) || defined(__riscv)
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_UNDEFINED_IMPL
#    endif
#elif defined(_MSC_VER)
/* TODO: Verify that this works on WINE and that indeed every windows compiler only works in little endian mode */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__POCC__)
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__CCC)
/*
 * https://statics.cirrus.com/pubs/manual/cirrus_c_compiler_um15.pdf#page=45
 * As one can see in the figure 7-4, the most significant bits are stored in the lowest address, therefore the byte
 * order is considered to be big endian.
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(__SDCC) || defined(SDCC)
/* https://sdcc.sourceforge.net/doc/sdccman.pdf#subsection.8.1.10 */
#    if defined(__SDCC_hc08) || defined(__SDCC_s08) || defined(__SDCC_stm8)
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#    else
#        define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#    endif
#elif defined(__IAR_SYSTEMS_ICC__)
#    ifdef __LITTLE_ENDIAN__
#        if __LITTLE_ENDIAN__
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#        else
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#        endif
#    else
/* WARNING: Really high possibility of errors here */
#        ifdef __ICCAVR__
/* https://wwwfiles.iar.com/AVR/webic/doc/EWAVR_CompilerGuide.pdf#G23.5679871 */
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#        elif defined(__ICCAVR32__)
/*
 * https://ww1.microchip.com/downloads/en/devicedoc/doc32000.pdf#G2.1028371
 * https://en.wikipedia.org/wiki/AVR32
 * https://wwwfiles.iar.com/AVR32/webic/doc/EWAVR32_CompilerReference.pdf#G22.2335038
 */
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#        elif defined(__ICC430__)
/*
 * https://en.wikipedia.org/wiki/TI_MSP430
 * https://wwwfiles.iar.com/msp430/webic/doc/EW430_CompilerReference.pdf (lacks macro)
 */
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#        elif defined(__ICCSTM8__)
/*
 * https://wwwfiles.iar.com/stm8/guides/EWSTM8_DevelopmentGuide.pdf#G23.2352216
 */
#            define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#        elif defined(__ICCARM__)
/*
 * https://wwwfiles.iar.com/arm/webic/doc/EWARM_DevelopmentGuide.ENU.pdf#G27.6055613
 * https://developer.arm.com/documentation/ihi0053/latest/
 */
#            if defined(__ARM_BIG_ENDIAN)
#                define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#            else
#                define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#            endif
#        elif defined(__ICC8051__)
/* https://wwwfiles.iar.com/8051/webic/doc/EW8051_MigrationGuide_v7.pdf#page=18 */
#            if (__VER__ / 100) <= 5
#                define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#            else
#                define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#            endif
#        else
/*
 *
 * TODO: IAR for RISC-V manual doesn't list __LITTLE_ENDIAN__ so... (Most probably it's little endian)
 */
#            error "Bitter: Cannot define endian using IAR compiler"
#        endif
#    endif
#elif defined(_XCARM_VER)
/* https://www.crossware.com/smanuals/cpparm/_t56.html */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(_XCCF_VER)
/* https://www.crossware.com/smanuals/cppcoldfire/_t56.html */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(_XC68K_VER)
/* https://www.crossware.com/smanuals/c680x0/_t56.html */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(_XC51_VER)
/* https://www.crossware.com/smanuals/c8051/_t237.html */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(_CC51)
/* https://www.tasking.com/support/8051/m_cc51_v7.2.pdf#page=75 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(__C51__) || defined(__CX51__)
/* https://developer.arm.com/documentation/101655/0961/Cx51-User-s-Guide/Appendix/E--Byte-Ordering */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_BIG_IMPL
#elif defined(__CODEVISIONAVR__)
/*
 * https://www.hpinfotech.ro/cvavr-documentation.html
 * Chapter 4.7   - Variables
 * Chapter 4.7.3 - Allocation of Variables to Registers
 * Chapter 4.17  - RAM Memory Organization and Register Allocation
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__XC8__) || defined(__XC8) || defined(__XC8_VERSION)
/*
 * The representation on both PIC and AVR is little-endian
 * PIC:
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC8-C-Compiler-Users-Guide-for-PIC-DS50002737.pdf#page=94
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC8-C-Compiler-Users-Guide-for-PIC-DS50002737.pdf#page=96
 * AVR:
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC8-C-Compiler-Users-Guide-for-AVR-MCU-50002750.pdf#page=66
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC8-C-Compiler-Users-Guide-for-AVR-MCU-50002750.pdf#page=67
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__XC16__) || defined(__XC16) || defined(XC16)
/*
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/50002071.pdf#page=108
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/50002071.pdf#page=109
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/50002071.pdf#page=110
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__XC_DSC) || defined(__XC_DSC__) || defined(__XC_DSC_VERSION__)
/*
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC-DSC-C-Compiler-User-Guide-DS50003589.pdf#page=87
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC-DSC-C-Compiler-User-Guide-DS50003589.pdf#page=88
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__XC32) || defined(__XC32__) || defined(__XC32_VERSION) || defined(__C32_VERSION)
/*
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC32-Compiler-UG-PIC32C-DS-50002895.pdf#page=114
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC32-Compiler-UG-PIC32C-DS-50002895.pdf#page=116
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC32-Compiler-UG-PIC32M-DS-50002799.pdf#page=128
 * https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/UserGuides/MPLAB-XC32-Compiler-UG-PIC32M-DS-50002799.pdf#page=130
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__18CXX)
/* https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_C18_Users_Guide_51288d.pdf#G5.626208 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__dsPIC30) || defined(__dsPIC30__) || defined(dsPIC30)
/* https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_C30_Users_Guide_51284c.pdf#G8.629954 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#elif defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_AMD64) \
    || defined(_M_X64) || defined(i386) || defined(__i386) || defined(__i386__) || defined(__i486__)          \
    || defined(__i586__) || defined(__i686__) || defined(__i386) || defined(__IA32__) || defined(_M_I86)      \
    || defined(_M_IX86) || defined(__X86__) || defined(_X86_) || defined(__THW_INTEL__) || defined(__I86__)   \
    || defined(__INTEL__) || defined(__386)
/*
 * FIXME: Someone on the git mailing list mentioned of a Big-Endian x86 platform.
 *        The person who made this claim is talking about HPE NonStop systems, so I'll
 *        to research how to query when the CPU arch is x86 but is running on NonStop.
 *        Maybe using the predefined macro set by tandem will suffice?
 *        https://lore.kernel.org/git/00a801da443d$b1539670$13fac350$@nexbridge.com/
 */
#    define BITTER_ENDIAN_NATIVE_IMPL BITTER_ENDIAN_LITTLE_IMPL
#else
#    error "TODO: Define endian for more platforms"
#endif

#endif /* BITTER_IMPL_ENDIAN_H */
