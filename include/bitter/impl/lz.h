/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_CLZ_H
#define BITTER_IMPL_CLZ_H

#include <bitter/detail/prelude.h>
#include <bitter/kernels/klz.h>

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_zeros_uc(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
    return __lzcnt16(BITTER_CAST(unsigned short, value))
           - BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH - BITTER_UCHAR_WIDTH);
#    else
    unsigned long result;
    if (!_BitScanReverse(&result, BITTER_CAST(unsigned long, value)))
        return BITTER_UCHAR_WIDTH;
    return BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* TODO: Research on whether the ternary is really necessary */
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm-intrinsics?view=msvc-170 */
    return value == 0u
               ? BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH)
               : (_CountLeadingZeros(value) - BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH - BITTER_UCHAR_WIDTH));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_leading_zeros)
    return __builtin_stdc_leading_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_clzg)
    return __builtin_clzg(value, BITTER_UCHAR_WIDTH);
#elif BITTER_HAS_BUILTIN(__builtin_clz) || (defined(__TINYC__) && (__TINYC__ > 927)) || defined(__COMPCERT__)
    return value == 0 ? BITTER_UCHAR_WIDTH : (__builtin_clz(value) - (BITTER_UINT_WIDTH - BITTER_UCHAR_WIDTH));
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0 ? BITTER_CAST(unsigned int, BITTER_UCHAR_WIDTH) : (BITTER_UCHAR_WIDTH - 1u - _bsr(value));
#else
    unsigned int res;
    BITTER_LZ_UC_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_zeros_us(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
    return __lzcnt16(value);
#    else
    unsigned long result;
    if (!_BitScanReverse(&result, BITTER_CAST(unsigned long, value)))
        return BITTER_USHRT_WIDTH;
    return BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* TODO: Research on whether the ternary is really necessary */
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm-intrinsics?view=msvc-170 */
    return value == 0u
               ? BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH)
               : (_CountLeadingZeros(value) - BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH - BITTER_USHRT_WIDTH));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_leading_zeros)
    return __builtin_stdc_leading_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_clzg)
    return __builtin_clzg(value, BITTER_USHRT_WIDTH);
#elif BITTER_HAS_BUILTIN(__builtin_clzs)
    return value == 0 ? BITTER_USHRT_WIDTH : __builtin_clzs(value);
#elif BITTER_HAS_BUILTIN(__builtin_clz)
    return value == 0 ? BITTER_USHRT_WIDTH : (__builtin_clz(value) - (BITTER_UINT_WIDTH - BITTER_USHRT_WIDTH));
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0 ? BITTER_CAST(unsigned int, BITTER_USHRT_WIDTH) : (BITTER_USHRT_WIDTH - 1u - _bsr(value));
#else
    unsigned int res;
    BITTER_LZ_US_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_zeros_ui(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
    return __lzcnt(value);
#    else
    unsigned long result;
    if (!_BitScanReverse(&result, BITTER_CAST(unsigned long, value)))
        return BITTER_UINT_WIDTH;
    return BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* TODO: Research on whether the ternary is really necessary */
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm-intrinsics?view=msvc-170 */
    return value == 0u
               ? BITTER_CAST(unsigned int, BITTER_UINT_WIDTH)
               : (_CountLeadingZeros(value) - BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH - BITTER_UINT_WIDTH));
#elif BITTER_HAS_BUILTIN(__builtin_stdc_leading_zeros)
    return __builtin_stdc_leading_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_clzg)
    return __builtin_clzg(value, BITTER_UINT_WIDTH);
#elif BITTER_HAS_BUILTIN(__builtin_clz)
    return value == 0u ? BITTER_UINT_WIDTH : __builtin_clz(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    return value == 0u ? BITTER_CAST(unsigned int, BITTER_UINT_WIDTH) : (BITTER_UINT_WIDTH - 1u - _bsr(value));
#else
    unsigned int res;
    BITTER_LZ_UI_IMPL(res, value);
    return res;
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_zeros_ul(value);
#elif defined(_MSC_VER)                                                                             \
    && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
        && !defined(_M_ARM64EC))                                                                    \
    && !defined(_M_CEE) && !defined(_MANAGED)
#    if defined(__AVX2__)
#        if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(
        sizeof(unsigned long) == sizeof(unsigned int), "[Bitter::LeadingZeros]: Function argument might be truncated!");
#        endif
    return __lzcnt(BITTER_CAST(unsigned int, value));
#    else
    unsigned long result;
    if (!_BitScanReverse(&result, value))
        return BITTER_ULONG_WIDTH;
    return BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#    endif
#elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
    && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* TODO: Research on whether the ternary is really necessary */
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm-intrinsics?view=msvc-170 */
    return value == 0ul ? BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH) : _CountLeadingZeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_leading_zeros)
    return __builtin_stdc_leading_zeros(value);
#elif BITTER_HAS_BUILTIN(__builtin_clzg)
    return __builtin_clzg(value, BITTER_ULONG_WIDTH);
#elif BITTER_HAS_BUILTIN(__builtin_clzl)
    return value == 0ul ? BITTER_ULONG_WIDTH : __builtin_clzl(value);
#elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    enum bitter_lz_long_test {
        bitter_lz_assert = sizeof(char[(sizeof(unsigned long) == sizeof(unsigned int)) ? 1 : -1])
    };
    return value == 0ul ? BITTER_CAST(unsigned int, BITTER_ULONG_WIDTH)
                        : (BITTER_ULONG_WIDTH - 1u - _bsr(BITTER_CAST(unsigned int, value)));
#else
    unsigned int res;
    BITTER_LZ_UL_IMPL(res, value);
    return res;
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_leading_zeros_ull(value);
#    elif defined(_MSC_VER)                                                                             \
        && ((defined(_M_IX86) || defined(_M_AMD64) || defined(_M_X64)) && !defined(_M_HYBRID_X86_ARM64) \
            && !defined(_M_ARM64EC))                                                                    \
        && !defined(_M_CEE) && !defined(_MANAGED)
#        if defined(__AVX2__)
#            ifdef _M_IX86
    unsigned int high;
    unsigned int low;

#                if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) > sizeof(unsigned int),
        "[Bitter::LeadingZeros]: Undefined behaviour due to shift with exponent larger than the width of unsigned long "
        "long would occur!");
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) <= 2 * sizeof(unsigned int),
        "[Bitter::LeadingZeros]: Two phase calculation of leading zeros would not work!");
#                endif

    high = BITTER_CAST(unsigned int, value >> BITTER_CAST(unsigned long long, BITTER_UINT_WIDTH));
    if (high != 0u)
        return __lzcnt(high) - BITTER_CAST(unsigned int, 2 * BITTER_UINT_WIDTH - BITTER_ULLONG_WIDTH);

    /* clang-format off */
    low = BITTER_CAST(unsigned int, value & BITTER_CAST(unsigned long long, UINT_MAX));
    /* clang-format on */
    return __lzcnt(low) + BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH - BITTER_UINT_WIDTH);
#            else
    return BITTER_CAST(unsigned int, __lzcnt64(value));
#            endif
#        else
#            ifdef _M_IX86
    unsigned long high;
    unsigned long low;
    unsigned long result;

#                if BITTER_HAS_STATIC_ASSERT
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) > sizeof(unsigned long),
        "[Bitter::LeadingZeros]: Undefined behaviour due to shift with exponent larger than the width of unsigned long "
        "long would occur!");
    BITTER_STATIC_ASSERT(sizeof(unsigned long long) <= 2 * sizeof(unsigned long),
        "[Bitter::LeadingZeros]: Two phase calculation of leading zeros would not work!");
#                endif

    high = BITTER_CAST(unsigned long, value >> BITTER_ULONG_WIDTH);
    if (_BitScanReverse(&result, high))
        return BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH - BITTER_ULONG_WIDTH) - 1u
               - BITTER_CAST(unsigned int, result);

    /* clang-format off */
    low = BITTER_CAST(unsigned long, value & BITTER_CAST(unsigned long long, ULONG_MAX));
    /* clang-format on */
    if (!_BitScanReverse(&result, low))
        return BITTER_ULLONG_WIDTH;

    return BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#            else
    unsigned long result;
    if (!_BitScanReverse64(&result, value))
        return BITTER_ULLONG_WIDTH;
    return BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) - 1u - BITTER_CAST(unsigned int, result);
#            endif
#        endif
#    elif (defined(_MSC_VER) && (_MSC_VER >= 1939)) && (defined(_M_ARM) || defined(_M_ARM64) || defined(_M_ARM64EC)) \
        && !defined(__clang__) && !defined(_M_CEE) && !defined(_MANAGED)
    /* TODO: Research on whether the ternary is really necessary */
    /* https://learn.microsoft.com/en-us/cpp/intrinsics/arm-intrinsics?view=msvc-170 */
    return value == 0ull ? BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) : _CountLeadingZeros64(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_leading_zeros)
    return __builtin_stdc_leading_zeros(value);
#    elif BITTER_HAS_BUILTIN(__builtin_clzg)
    return __builtin_clzg(value, BITTER_ULLONG_WIDTH);
#    elif BITTER_HAS_BUILTIN(__builtin_clzll)
    return value == 0ull ? BITTER_ULLONG_WIDTH : __builtin_clzll(value);
#    elif defined(__LCC__) && defined(_WIN32) && !defined(__POCC__)
    unsigned int high;
    unsigned int low;
    unsigned int result;

    high = BITTER_CAST(unsigned int, value >> BITTER_UINT_WIDTH);
    if (high != 0u)
        return BITTER_UINT_WIDTH - 1u - _bsr(high);

    low = (unsigned int)(value & UINT_MAX);
    return low == 0u ? BITTER_CAST(unsigned int, BITTER_ULLONG_WIDTH) : (BITTER_ULLONG_WIDTH - 1u - _bsr(low));
#    else
    unsigned int res;
    BITTER_LZ_ULL_IMPL(res, value);
    return res;
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_CLZ_H */
