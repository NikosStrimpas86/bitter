/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_FLO_H
#define BITTER_IMPL_FLO_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_one_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_one)
    return __builtin_stdc_first_leading_one(value);
#else
    return value == 0 ? 0u : (bitter_leading_zeros_uc(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_one_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_one)
    return __builtin_stdc_first_leading_one(value);
#else
    return value == 0 ? 0u : (bitter_leading_zeros_us(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_one_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_one)
    return __builtin_stdc_first_leading_one(value);
#else
    return value == 0u ? 0u : (bitter_leading_zeros_ui(value) + 1u);
#endif
}

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_one_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_one)
    return __builtin_stdc_first_leading_one(value);
#else
    return value == 0ul ? 0u : (bitter_leading_zeros_ul(value) + 1u);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_first_leading_one_ull(value);
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_first_leading_one)
    return __builtin_stdc_first_leading_one(value);
#    else
    return value == 0ull ? 0u : (bitter_leading_zeros_ull(value) + 1u);
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_FLO_H */
