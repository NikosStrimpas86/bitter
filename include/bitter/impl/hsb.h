/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_IMPL_HSB_H
#define BITTER_IMPL_HSB_H

#include <bitter/detail/prelude.h>

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_has_single_bit_uc(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_has_single_bit)
    /* https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#index-_005f_005fbuiltin_005fstdc_005fhas_005fsingle_005fbit */
    return __builtin_stdc_has_single_bit(value);
#else
    unsigned int const promoted_value = BITTER_CAST(unsigned int, value);
    return (promoted_value ^ (promoted_value - 1u)) > (promoted_value - 1u);
#endif
}

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_has_single_bit_us(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_has_single_bit)
    /* https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#index-_005f_005fbuiltin_005fstdc_005fhas_005fsingle_005fbit */
    return __builtin_stdc_has_single_bit(value);
#else
    unsigned int const promoted_value = BITTER_CAST(unsigned int, value);
    return (promoted_value ^ (promoted_value - 1u)) > (promoted_value - 1u);
#endif
}

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_has_single_bit_ui(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_has_single_bit)
    /* https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#index-_005f_005fbuiltin_005fstdc_005fhas_005fsingle_005fbit */
    return __builtin_stdc_has_single_bit(value);
#else
    return (value ^ (value - 1u)) > (value - 1u);
#endif
}

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#if BITTER_HAS_STDBIT_HEADER
    return stdc_has_single_bit_ul(value);
#elif BITTER_HAS_BUILTIN(__builtin_stdc_has_single_bit)
    /* https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#index-_005f_005fbuiltin_005fstdc_005fhas_005fsingle_005fbit */
    return __builtin_stdc_has_single_bit(value);
#else
    return (value ^ (value - 1ul)) > (value - 1ul);
#endif
}

#if BITTER_HAS_UNSIGNED_LONG_LONG

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT
{
#    if BITTER_HAS_STDBIT_HEADER
    return stdc_has_single_bit_ull(value)
#    elif BITTER_HAS_BUILTIN(__builtin_stdc_has_single_bit)
    /* https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#index-_005f_005fbuiltin_005fstdc_005fhas_005fsingle_005fbit */
    return __builtin_stdc_has_single_bit(value);
#    else
    return (value ^ (value - 1ull)) > (value - 1ull);
#    endif
}

#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#endif /* BITTER_IMPL_HSB_H */
