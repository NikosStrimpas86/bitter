/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_DETAIL_UTIL_H
#define BITTER_DETAIL_UTIL_H

#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        define BITTER_HAS_VARIADIC_MACROS 1
#    else
#        define BITTER_HAS_VARIADIC_MACROS 0
#    endif
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 199901L
#        define BITTER_HAS_VARIADIC_MACROS 1
#    else
#        define BITTER_HAS_VARIADIC_MACROS 0
#    endif
#else
#    define BITTER_HAS_VARIADIC_MACROS 0
#endif

#ifdef __has_include
#    define BITTER_HAS_INCLUDE __has_include
#else
#    define BITTER_HAS_INCLUDE(header) 0
#endif

#ifdef __has_builtin
#    define BITTER_HAS_BUILTIN __has_builtin
#else
#    define BITTER_HAS_BUILTIN(builtin) 0
#endif

#if defined(__has_c_attribute) && defined(__STDC_VERSION__)
/*
 * GCC before version 14, cannot query scoped attributes in older versions of C
 * https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=37127ed975e09813eaa2d1cf1062055fce45dd16
 */
#    if __STDC_VERSION__ > 201710L
#        define BITTER_HAS_C_ATTRIBUTE __has_c_attribute
#    else
#        define BITTER_HAS_C_ATTRIBUTE 0
#    endif
#else
#    define BITTER_HAS_C_ATTRIBUTE(attr) 0
#endif

#if defined(__has_cpp_attribute) && defined(__cpluslus)
/*
 * GCC before version 14, cannot query scoped attributes in older versions of C++
 * https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=37127ed975e09813eaa2d1cf1062055fce45dd16
 */
#    if __cplusplus >= 201103L
#        define BITTER_HAS_CPP_ATTRIBUTE __has_cpp_attribute
#    else
#        define BITTER_HAS_CPP_ATTRIBUTE(attr) 0
#    endif
#else
#    define BITTER_HAS_CPP_ATTRIBUTE(attr) 0
#endif

#ifdef __has_attribute
#    define BITTER_HAS_ATTRIBUTE __has_attribute
#else
#    define BITTER_HAS_ATTRIBUTE(attr) 0
#endif

#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        define BITTER_NOEXCEPT noexcept
#    elif BITTER_HAS_ATTRIBUTE(nothrow)
#        define BITTER_NOEXCEPT __attribute__((nothrow))
#    else
#        define BITTER_NOEXCEPT throw()
#    endif
#else
#    define BITTER_NOEXCEPT /* noexcept */
#endif

#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        define BITTER_HAS_STATIC_ASSERT 1
#        define BITTER_STATIC_ASSERT     static_assert
#    else
#        define BITTER_HAS_STATIC_ASSERT 0
#    endif
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 202311L
#        define BITTER_HAS_STATIC_ASSERT 1
#        define BITTER_STATIC_ASSERT     static_assert
#    elif __STDC_VERSION__ >= 201112L
#        define BITTER_HAS_STATIC_ASSERT 1
#        define BITTER_STATIC_ASSERT     _Static_assert
#    else
#        define BITTER_HAS_STATIC_ASSERT 0
#    endif
#else
#    define BITTER_HAS_STATIC_ASSERT 0
#endif

#ifdef __cplusplus
#    if (__cplusplus >= 202002L) && BITTER_HAS_INCLUDE(<version>) && BITTER_HAS_INCLUDE(<bit>)
#        include <version>
#        ifdef __cpp_lib_bitops
#            if __cpp_lib_bitops >= 201907L
#                define BITTER_HAS_BIT_HEADER 1
#            else
#                define BITTER_HAS_BIT_HEADER 0
#            endif
#        else
#            define BITTER_HAS_BIT_HEADER 0
#        endif
#    else
#        define BITTER_HAS_BIT_HEADER 0
#    endif
#else
#    define BITTER_HAS_BIT_HEADER 0
#endif

#ifdef __STDC_VERSION__
#    if (__STDC_VERSION__ > 201710L) && BITTER_HAS_INCLUDE(<stdbit.h>)
#        include <stdbit.h>
#        ifdef __STDC_VERSION_STDBIT_H__
#            if __STDC_VERSION_STDBIT_H__ >= 202311L
#                define BITTER_HAS_STDBIT_HEADER 1
#            else
#                define BITTER_HAS_STDBIT_HEADER 0
#            endif
#        else
#            define BITTER_HAS_STDBIT_HEADER 0
#        endif
#    else
#        define BITTER_HAS_STDBIT_HEADER 0
#    endif
#else
#    define BITTER_HAS_STDBIT_HEADER 0
#endif

#ifdef __cplusplus
#    if BITTER_HAS_VARIADIC_MACROS
#        define BITTER_CAST(to, ...) (static_cast<to>(__VA_ARGS__))
#    else
#        define BITTER_CAST(to, val) (static_cast<to>(val))
#    endif
#else
#    if BITTER_HAS_VARIADIC_MACROS
#        define BITTER_CAST(to, ...) ((to)(__VA_ARGS__))
#    else
#        define BITTER_CAST(to, val) ((to)(val))
#    endif
#endif

#if BITTER_HAS_BUILTIN(__builtin_expect)
#    define BITTER_UNLIKELY(cond) __builtin_expect((cond), 0)
#else
#    define BITTER_UNLIKELY(cond) (cond)
#endif

/*
 * NOTE: Workaround, due to MSVC throwing warnings on attribute query expressions:
 *       "unexpected tokens following preprocessor directive - expected a newline"
 */
#if defined(_MSC_VER) && !defined(__clang__)
#    define BITTER_UNSEQ_FNT  /* unsequenced */
#    define BITTER_UNSEQ_DECL /* unsequenced */
#else
#    if BITTER_HAS_C_ATTRIBUTE(__unsequenced__)
#        define BITTER_UNSEQ_FNT  [[__unsequenced__]]
#        define BITTER_UNSEQ_DECL /* unsequenced */
#    elif BITTER_HAS_C_ATTRIBUTE(__gnu__::__const__) || BITTER_HAS_CPP_ATTRIBUTE(__gnu__::__const__)
#        define BITTER_UNSEQ_FNT  /* unsequenced */
#        define BITTER_UNSEQ_DECL [[__gnu__::__const__]]
#    elif BITTER_HAS_ATTRIBUTE(__const__)
#        define BITTER_UNSEQ_FNT  /* unsequenced */
#        define BITTER_UNSEQ_DECL __attribute__((__const__))
#    elif defined(__CC_NORCROFT) || defined(__ARMCC_VERSION)
#        define BITTER_UNSEQ_FNT  __pure
#        define BITTER_UNSEQ_DECL /* unsequenced */
#    else
#        define BITTER_UNSEQ_FNT  /* unsequenced */
#        define BITTER_UNSEQ_DECL /* unsequenced */
#    endif
#endif

#endif /* BITTER_DETAIL_UTIL_H */
