/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_DETAIL_TYPES_H
#define BITTER_DETAIL_TYPES_H

/*
 * If the toolchain doesn't provide these, there is nothing we can do
 */
#include <limits.h>
#include <stddef.h>
#ifdef __cplusplus
#    if __cplusplus >= 201103L
#        include <stdint.h>
#        define BITTER_HAS_STDINT_HEADER 1
#    else
#        define BITTER_HAS_STDINT_HEADER 0
#    endif
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 199901L
#        include <stdint.h>
#        define BITTER_HAS_STDINT_HEADER 1
#    else
#        define BITTER_HAS_STDINT_HEADER 0
#    endif
#else
#    define BITTER_HAS_STDINT_HEADER 0
#endif

/* FIXME: Some small compilers don't support long long even in C99 (or later) mode */
#ifdef __cplusplus
#    if (__cplusplus >= 201103L) && (defined(ULLONG_MAX || defined(ULLONG_WIDTH))
#        define BITTER_HAS_UNSIGNED_LONG_LONG 1
#    else
#        define BITTER_HAS_UNSIGNED_LONG_LONG 0
#    endif
#elif defined(__STDC_VERSION__)
#    if (__STDC_VERSION__ >= 199901L) && (defined(ULLONG_MAX) || defined(ULLONG_WIDTH)) && !defined(__ANSIC__ONLY__)
#        define BITTER_HAS_UNSIGNED_LONG_LONG 1
#    else
#        define BITTER_HAS_UNSIGNED_LONG_LONG 0
#    endif
#else
#    define BITTER_HAS_UNSIGNED_LONG_LONG 0
#endif

#define BITTER_HAS_SIZE 1

#if BITTER_HAS_STDINT_HEADER

#    if defined(UINT8_MAX) || defined(UINT8_WIDTH)
#        define BITTER_HAS_UINT8 1
#    else
#        define BITTER_HAS_UINT8 0
#    endif

#    if defined(UINT16_MAX) || defined(UINT16_WIDTH)
#        define BITTER_HAS_UINT16 1
#    else
#        define BITTER_HAS_UINT16 0
#    endif

#    if defined(UINT32_MAX) || defined(UINT32_WIDTH)
#        define BITTER_HAS_UINT32 1
#    else
#        define BITTER_HAS_UINT32 0
#    endif

#    if defined(UINT64_MAX) || defined(UINT64_WIDTH)
#        define BITTER_HAS_UINT64 1
#    else
#        define BITTER_HAS_UINT64 0
#    endif

#    if defined(UINT128_MAX) || defined(UINT128_WIDTH)
#        define BITTER_HAS_UINT128 1
#    else
#        define BITTER_HAS_UINT128 0
#    endif

#    if defined(UINT_LEAST8_MAX) || defined(UINT_LEAST8_WIDTH)
#        define BITTER_HAS_UINT_LEAST8 1
#    else
#        define BITTER_HAS_UINT_LEAST8 0
#    endif

#    if defined(UINT_LEAST16_MAX) || defined(UINT_LEAST16_WIDTH)
#        define BITTER_HAS_UINT_LEAST16 1
#    else
#        define BITTER_HAS_UINT_LEAST16 0
#    endif

#    if defined(UINT_LEAST32_MAX) || defined(UINT_LEAST32_WIDTH)
#        define BITTER_HAS_UINT_LEAST32 1
#    else
#        define BITTER_HAS_UINT_LEAST32 0
#    endif

#    if defined(UINT_LEAST64_MAX) || defined(UINT_LEAST64_WIDTH)
#        define BITTER_HAS_UINT_LEAST64 1
#    else
#        define BITTER_HAS_UINT_LEAST64 0
#    endif

#    if defined(UINT_LEAST128_MAX) || defined(UINT_LEAST128_WIDTH)
#        define BITTER_HAS_UINT_LEAST128 1
#    else
#        define BITTER_HAS_UINT_LEAST128 0
#    endif

#    if defined(UINT_FAST8_MAX) || defined(UINT_FAST8_WIDTH)
#        define BITTER_HAS_UINT_FAST8 1
#    else
#        define BITTER_HAS_UINT_FAST8 0
#    endif

#    if defined(UINT_FAST16_MAX) || defined(UINT_FAST16_WIDTH)
#        define BITTER_HAS_UINT_FAST16 1
#    else
#        define BITTER_HAS_UINT_FAST16 0
#    endif

#    if defined(UINT_FAST32_MAX) || defined(UINT_FAST32_WIDTH)
#        define BITTER_HAS_UINT_FAST32 1
#    else
#        define BITTER_HAS_UINT_FAST32 0
#    endif

#    if defined(UINT_FAST64_MAX) || defined(UINT_FAST64_WIDTH)
#        define BITTER_HAS_UINT_FAST64 1
#    else
#        define BITTER_HAS_UINT_FAST64 0
#    endif

#    if defined(UINT_FAST128_MAX) || defined(UINT_FAST128_WIDTH)
#        define BITTER_HAS_UINT_FAST128 1
#    else
#        define BITTER_HAS_UINT_FAST128 0
#    endif

#    if defined(UINTPTR_MAX) || defined(UINTPTR_WIDTH)
#        define BITTER_HAS_UINTPTR 1
#    else
#        define BITTER_HAS_UINTPTR 0
#    endif

#    if defined(UINTMAX_MAX) || defined(UINTMAX_WIDTH)
#        define BITTER_HAS_UINTMAX 1
#    else
#        define BITTER_HAS_UINTMAX 0
#    endif

#endif

#ifdef __18CXX
#    define BITTER_HAS_UNSIGNED_SHORT_LONG 1
#else
#    define BITTER_HAS_UNSIGNED_SHORT_LONG 0
#endif

#if !defined(__STDC_VERSION__) && (defined(_MSC_VER) || defined(__MINGW32__) || defined(__MINGW64__) || defined(__ICL))
#    define BITTER_HAS_EXTENDED_INT64          1
#    define BITTER_HAS_EXTENDED_UNSIGNED_INT64 1
#else
#    define BITTER_HAS_EXTENDED_INT64          0
#    define BITTER_HAS_EXTENDED_UNSIGNED_INT64 0
#endif

#ifdef __SIZEOF_INT128__
#    define BITTER_HAS_EXTENDED_INT128          1
#    define BITTER_HAS_EXTENDED_UNSIGNED_INT128 1
#else
#    define BITTER_HAS_EXTENDED_INT128          0
#    define BITTER_HAS_EXTENDED_UNSIGNED_INT128 0
#endif

#ifdef __cplusplus
#    define BITTER_BOOL_IMPL bool
#    define BITTER_TRUE      true
#    define BITTER_FALSE     false
#elif defined(__STDC_VERSION__)
#    if __STDC_VERSION__ >= 202311L
#        define BITTER_BOOL_IMPL bool
#        define BITTER_TRUE      true
#        define BITTER_FALSE     false
#    elif __STDC_VERSION__ >= 199901L
#        define BITTER_BOOL_IMPL _Bool
#        define BITTER_TRUE      1
#        define BITTER_FALSE     0
#    else
#        define BITTER_BOOL_IMPL unsigned char
#        define BITTER_TRUE      1
#        define BITTER_FALSE     0
#    endif
#else
#    define BITTER_BOOL_IMPL unsigned char
#    define BITTER_TRUE      1
#    define BITTER_FALSE     0
#endif

typedef BITTER_BOOL_IMPL bitter_bool;

#endif /* BITTER_DETAIL_TYPES_H */
