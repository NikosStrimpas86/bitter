/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_DETAIL_WIDTHS_H
#define BITTER_DETAIL_WIDTHS_H

#include <bitter/detail/types.h>
#include <bitter/detail/util.h>

#ifndef CHAR_BIT
#    error "[Bitter::Detail::Widths]: CHAR_BIT is not defined!"
#endif /* CHAR_BIT */

#ifndef UCHAR_MAX
#    error "[Bitter::Detail::Widths]: UCHAR_MAX is not defined!"
#endif /* UCHAR_MAX */

#ifndef USHRT_MAX
#    error "[Bitter::Detail::Widths]: USHRT_MAX is not defined!"
#endif /* USHRT_MAX */

#ifndef UINT_MAX
#    error "[Bitter::Detail::Widths]: UINT_MAX is not defined!"
#endif /* UINT_MAX */

#ifndef ULONG_MAX
#    error "[Bitter::Detail::Widths]: ULONG_MAX is not defined!"
#endif /* ULONG_MAX */

#if BITTER_HAS_UNSIGNED_LONG_LONG
#    ifndef ULLONG_MAX
#        error "[Bitter::Detail::Widths]: ULLONG_MAX is not defined!"
#    endif /* ULLONG_MAX */
#endif     /* BITTER_HAS_UNSIGNED_LONG_LONG */

/* unsigned char */
#ifdef UCHAR_WIDTH
#    define BITTER_UCHAR_WIDTH UCHAR_WIDTH
#elif defined(__UCHAR_WIDTH__)
#    define BITTER_UCHAR_WIDTH __UCHAR_WIDTH__
#elif defined(__SCHAR_WIDTH__)
#    define BITTER_UCHAR_WIDTH __SCHAR_WIDTH__
#else
#    define BITTER_UCHAR_WIDTH CHAR_BIT
#endif

/* unsigned short */
#ifdef USHRT_WIDTH
#    define BITTER_USHRT_WIDTH USHRT_WIDTH
#elif defined(__USHRT_WIDTH__)
#    define BITTER_USHRT_WIDTH __USHRT_WIDTH__
#elif defined(__SHRT_WIDTH__)
#    define BITTER_USHRT_WIDTH __SHRT_WIDTH__
#elif defined(_ILP32) || defined(__ILP32__) || defined(__ILP32)
#    define BITTER_USHRT_WIDTH 16
#elif defined(_LP64) || defined(__LP64__) || defined(__LP64)
#    define BITTER_USHRT_WIDTH 16
#elif defined(_MSC_VER)
/* https://learn.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=msvc-170 */
#    define BITTER_USHRT_WIDTH 16
#elif defined(__LCC__) && defined(_WIN32)
/* https://lcc-win32.services.net/C-Tutorial.pdf#page=32 */
#    define BITTER_USHRT_WIDTH 16
#elif defined(__SDCC) || defined(SDCC)
/* https://sdcc.sourceforge.net/doc/sdccman.pdf#page=9 */
#    define BITTER_USHRT_WIDTH 16
#elif defined(__18CXX)
/* https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_C18_Users_Guide_51288d.pdf#G5.629107 */
#    define BITTER_USHRT_WIDTH 16
#elif defined(__CCC)
/* https://statics.cirrus.com/pubs/manual/cirrus_c_compiler_um15.pdf#G4.1268758 */
#    define BITTER_USHRT_WIDTH 32
#elif defined(__CODEVISIONAVR__)
/*
 * https://www.hpinfotech.ro/cvavr-documentation.html
 * Chapter 4.5 - Data Types
 */
#    define BITTER_USHRT_WIDTH 16
#elif defined(__AVR__) && (UINT_MAX == 0xff)
/* https://gcc.gnu.org/wiki/avr-gcc#Deviations_from_the_Standard */
#    define BITTER_USHRT_WIDTH 8
#else
#    if USHRT_MAX == 0xffff
#        define BITTER_USHRT_WIDTH 16
#    else
#        if USHRT_MAX == 0xfffff
#            define BITTER_USHRT_WIDTH 20
#        else
#            if USHRT_MAX == 0xffffff
#                define BITTER_USHRT_WIDTH 24
#            else
#                if USHRT_MAX == 0xfffffff
#                    define BITTER_USHRT_WIDTH 28
#                else
#                    if USHRT_MAX == 0xffffffff
#                        define BITTER_USHRT_WIDTH 32
#                    else
/* BITTER_USHRT_WIDTH is not usable in the preprocessor */
#                        define BITTER_USHRT_WIDTH      (sizeof(unsigned short) * CHAR_BIT)
#                        define BITTER_USHRT_WIDTH_NPPC 1
#                    endif
#                endif
#            endif
#        endif
#    endif
#endif

/* unsigned int */
#ifdef UINT_WIDTH
#    define BITTER_UINT_WIDTH UINT_WIDTH
#elif defined(__UINT_WIDTH__)
#    define BITTER_UINT_WIDTH __UINT_WIDTH__
#elif defined(__INT_WIDTH__)
#    define BITTER_UINT_WIDTH __INT_WIDTH__
#elif defined(_ILP32) || defined(__ILP32__) || defined(__ILP32)
#    define BITTER_UINT_WIDTH 32
#elif defined(_LP64) || defined(__LP64__) || defined(__LP64)
#    define BITTER_UINT_WIDTH 32
#elif defined(_MSC_VER)
/* https://learn.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=msvc-170 */
#    define BITTER_UINT_WIDTH 32
#elif defined(__LCC__) && defined(_WIN32)
/* https://lcc-win32.services.net/C-Tutorial.pdf#page=32 */
#    define BITTER_UINT_WIDTH 32
#elif defined(__SDCC) || defined(SDCC)
/* https://sdcc.sourceforge.net/doc/sdccman.pdf#page=9 */
#    define BITTER_UINT_WIDTH 16
#elif defined(__18CXX)
/* https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_C18_Users_Guide_51288d.pdf#G5.629107 */
#    define BITTER_UINT_WIDTH 16
#elif defined(__CCC)
/* https://statics.cirrus.com/pubs/manual/cirrus_c_compiler_um15.pdf#G4.1268758 */
#    define BITTER_UINT_WIDTH 32
#elif defined(__CODEVISIONAVR__)
/*
 * https://www.hpinfotech.ro/cvavr-documentation.html
 * Chapter 4.5 - Data Types
 */
#    define BITTER_UINT_WIDTH 16
#elif defined(__AVR__) && (UINT_MAX == 0xff)
/* https://gcc.gnu.org/wiki/avr-gcc#Deviations_from_the_Standard */
#    define BITTER_UINT_WIDTH 8
#else
#    if UINT_MAX == 0xffff
#        define BITTER_UINT_WIDTH 16
#    elif UINT_MAX == 0xfffff
#        define BITTER_UINT_WIDTH 20
#    elif UINT_MAX == 0xffffff
#        define BITTER_UINT_WIDTH 24
#    elif UINT_MAX == 0xfffffff
#        define BITTER_UINT_WIDTH 28
#    elif UINT_MAX == 0xffffffff
#        define BITTER_UINT_WIDTH 32
#    else
/* BITTER_UINT_WIDTH is not usable in the preprocessor */
#        define BITTER_UINT_WIDTH      (sizeof(unsigned short) * CHAR_BIT)
#        define BITTER_UINT_WIDTH_NPPC 1
#    endif
#endif

/* unsigned long */
#ifdef ULONG_WIDTH
#    define BITTER_ULONG_WIDTH ULONG_WIDTH
#elif defined(__ULONG_WIDTH__)
#    define BITTER_ULONG_WIDTH __ULONG_WIDTH__
#elif defined(__LONG_WIDTH__)
#    define BITTER_ULONG_WIDTH __LONG_WIDTH__
#elif defined(_ILP32) || defined(__ILP32__) || defined(__ILP32)
#    define BITTER_ULONG_WIDTH 32
#elif defined(_LP64) || defined(__LP64__) || defined(__LP64)
#    define BITTER_ULONG_WIDTH 64
#elif defined(_MSC_VER)
/* https://learn.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=msvc-170 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__LCC__) && defined(_WIN32)
/* https://lcc-win32.services.net/C-Tutorial.pdf#page=32 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__SDCC) || defined(SDCC)
/* https://sdcc.sourceforge.net/doc/sdccman.pdf#page=9 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__18CXX)
/* https://ww1.microchip.com/downloads/en/DeviceDoc/MPLAB_C18_Users_Guide_51288d.pdf#G5.629107 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__CCC)
/* https://statics.cirrus.com/pubs/manual/cirrus_c_compiler_um15.pdf#G4.1268758 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__CODEVISIONAVR__)
/*
 * https://www.hpinfotech.ro/cvavr-documentation.html
 * Chapter 4.5 - Data Types
 */
#    define BITTER_ULONG_WIDTH 32
#elif defined(__AVR__) && (UINT_MAX == 0xff)
/* https://gcc.gnu.org/wiki/avr-gcc#Deviations_from_the_Standard */
#    define BITTER_ULONG_WIDTH 16
#else
#    if ULONG_MAX == 0xfffffffful
#        define BITTER_ULONG_WIDTH 32
#    elif ULONG_MAX == 0xffffffffful
#        define BITTER_ULONG_WIDTH 36
#    elif ULONG_MAX == 0xfffffffffful
#        define BITTER_ULONG_WIDTH 40
#    elif ULONG_MAX == 0xffffffffffful
#        define BITTER_ULONG_WIDTH 44
#    elif ULONG_MAX == 0xfffffffffffful
#        define BITTER_ULONG_WIDTH 48
#    elif ULONG_MAX == 0xffffffffffffful
#        define BITTER_ULONG_WIDTH 52
#    elif ULONG_MAX == 0xfffffffffffffful
#        define BITTER_ULONG_WIDTH 56
#    elif ULONG_MAX == 0xffffffffffffffful
#        define BITTER_ULONG_WIDTH 60
#    elif ULONG_MAX == 0xfffffffffffffffful
#        define BITTER_ULONG_WIDTH 64
#    else
/* BITTER_ULONG_WIDTH is not usable in the preprocessor */
#        define BITTER_ULONG_WIDTH      (sizeof(unsigned long) * CHAR_BIT)
#        define BITTER_ULONG_WIDTH_NPPC 1
#    endif
#endif

#if BITTER_HAS_UNSIGNED_LONG_LONG
/* unsigned long long */

#    ifdef ULLONG_WIDTH
#        define BITTER_ULLONG_WIDTH ULLONG_WIDTH
#    elif defined(__ULLONG_WIDTH__)
#        define BITTER_ULLONG_WIDTH __ULLONG_WIDTH__
#    elif defined(__LLONG_WIDTH__)
#        define BITTER_ULLONG_WIDTH __LLONG_WIDTH__
#    elif defined(__LONG_LONG_WIDTH__)
#        define BITTER_ULLONG_WIDTH __LONG_LONG_WIDTH__
#    elif defined(_LP64) || defined(__LP64__) || defined(__LP64)
#        define BITTER_ULLONG_WIDTH 64
#    elif defined(_MSC_VER)
/* https://learn.microsoft.com/en-us/cpp/cpp/data-type-ranges?view=msvc-170 */
#        define BITTER_ULLONG_WIDTH 64
#    elif defined(__LCC__) && defined(_WIN32)
/* https://lcc-win32.services.net/C-Tutorial.pdf#page=32 */
#        define BITTER_ULLONG_WIDTH 64
#    elif defined(__SDCC) || defined(SDCC)
/* https://sdcc.sourceforge.net/doc/sdccman.pdf#page=9 */
#        define BITTER_ULLONG_WIDTH 64
#    elif defined(__CCC)
/* https://statics.cirrus.com/pubs/manual/cirrus_c_compiler_um15.pdf#G4.1268758 */
#        define BITTER_ULLONG_WIDTH 32
#    elif defined(__AVR__) && (UINT_MAX == 0xff)
/* https://gcc.gnu.org/wiki/avr-gcc#Deviations_from_the_Standard */
#        define BITTER_ULLONG_WIDTH 32
#    else
/* Cirrus-Logic compiler does this unfortunately */
#        if ULLONG_MAX == 0xffffffffull
#            define BITTER_ULLONG_WIDTH 32
#        elif ULLONG_MAX == 0xffffffffffffffffull
#            define BITTER_ULLONG_WIDTH 64
#        else
/* BITTER_ULLONG_WIDTH is not usable in the preprocessor */
#            define BITTER_ULLONG_WIDTH      (sizeof(unsigned long long) * CHAR_BIT)
#            define BITTER_ULLONG_WIDTH_NPPC 1
#        endif
#    endif

#endif

#if BITTER_HAS_STATIC_ASSERT
BITTER_STATIC_ASSERT((sizeof(unsigned char) * CHAR_BIT) >= BITTER_UCHAR_WIDTH,
    "[Bitter::Detail::Widths]: Error in definition of BITTER_UCHAR_WIDTH");
BITTER_STATIC_ASSERT((sizeof(unsigned short) * CHAR_BIT) >= BITTER_USHRT_WIDTH,
    "[Bitter::Detail::Widths]: Error in definition of BITTER_USHRT_WIDTH");
BITTER_STATIC_ASSERT((sizeof(unsigned int) * CHAR_BIT) >= BITTER_UINT_WIDTH,
    "[Bitter::Detail::Widths]: Error in definition of BITTER_UINT_WIDTH");
BITTER_STATIC_ASSERT((sizeof(unsigned long) * CHAR_BIT) >= BITTER_ULONG_WIDTH,
    "[Bitter::Detail::Widths]: Error in definition of BITTER_ULONG_WIDTH");
#    if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_STATIC_ASSERT((sizeof(unsigned long long) * CHAR_BIT) >= BITTER_ULLONG_WIDTH,
    "[Bitter::Detail::Widths]: Error in definition of BITTER_ULLONG_WIDTH");
#    endif /* BITTER_HAS_UNSIGNED_LONG_LONG */
#endif     /* BITTER_HAS_STATIC_ASSERT */

#endif /* BITTER_DETAIL_WIDTHS_H */
