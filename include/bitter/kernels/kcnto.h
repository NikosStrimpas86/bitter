/*
 * Copyright (c) 2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_KERNELS_KCNTO_H
#define BITTER_KERNELS_KCNTO_H

#include <bitter/detail/types.h>
#include <bitter/detail/util.h>
#include <bitter/detail/widths.h>

/* Count Ones */

/*
 * https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
 * This method was chosen because it doesn't generate runtime calls for
 * basic operations on the most architectures.
 * https://godbolt.org/z/1hqxzso7W
 */
#define BITTER_CNTO_8_BIT_KERNEL(                                                                        \
    resref, value, width, constwrapper, castwrapper, indexwrapper, type, promotedtype, indextype)        \
    do {                                                                                                 \
        type temp = castwrapper(value) - ((castwrapper(value) >> constwrapper(1)) & constwrapper(0x55)); \
        temp = (castwrapper(temp) & constwrapper(0x33))                                                  \
               + ((castwrapper(temp) >> constwrapper(2)) & constwrapper(0x33));                          \
        (resref) = BITTER_CAST(type, (castwrapper(temp) * constwrapper(0x11))) >> castwrapper(4);        \
    } while (0)

/*
 * TODO: For 32-bit (and higher) platforms, use 32-bit multiplication to calculate the bits set in an 8-bit integer.
 */

/*
 * TODO: Create Kernels for 16, 32, 64 and 128 bits.
 */

#define BITTER_CNTO_PORTABLE_KERNEL(                                                                \
    resref, value, width, constwrapper, castwrapper, indexwrapper, type, promotedtype, indextype)   \
    do {                                                                                            \
        indextype idx;                                                                              \
        (resref) = 0u;                                                                              \
        for (idx = constwrapper(0); idx < constwrapper(width); idx++) {                             \
            if ((castwrapper(value) & (constwrapper(1) << indexwrapper(idx))) != constwrapper(0)) { \
                (resref)++;                                                                         \
            }                                                                                       \
        }                                                                                           \
    } while (0)

#if BITTER_UCHAR_WIDTH == 8
#    define BITTER_CNTO_UC_CONST_WRAPPER_(a) a
#    define BITTER_CNTO_UC_CONST_WRAPPER(a)  BITTER_CNTO_UC_CONST_WRAPPER_(a)
#    define BITTER_CNTO_UC_CAST_WRAPPER(a)   a
#    define BITTER_CNTO_UC_INDEX_WRAPPER(a)  BITTER_CNTO_UC_CAST_WRAPPER(a)
#    define BITTER_CNTO_UC_IMPL(resref, value)                                                    \
        BITTER_CNTO_8_BIT_KERNEL(resref, value, BITTER_UCHAR_WIDTH, BITTER_CNTO_UC_CONST_WRAPPER, \
            BITTER_CNTO_UC_CAST_WRAPPER, BITTER_CNTO_UC_INDEX_WRAPPER, unsigned char, unsigned int, unsigned int)
#else
#    define BITTER_CNTO_UC_CONST_WRAPPER_(a) a##u
#    define BITTER_CNTO_UC_CONST_WRAPPER(a)  BITTER_CNTO_UC_CONST_WRAPPER_(a)
#    define BITTER_CNTO_UC_CAST_WRAPPER(a)   BITTER_CAST(unsigned int, a)
#    define BITTER_CNTO_UC_INDEX_WRAPPER(a)  a
#    define BITTER_CNTO_UC_IMPL(resref, value)                                                       \
        BITTER_CNTO_PORTABLE_KERNEL(resref, value, BITTER_UCHAR_WIDTH, BITTER_CNTO_UC_CONST_WRAPPER, \
            BITTER_CNTO_UC_CAST_WRAPPER, BITTER_CNTO_UC_INDEX_WRAPPER, unsigned char, unsigned int, unsigned int)
#endif

#define BITTER_CNTO_US_CONST_WRAPPER_(a) a##u
#define BITTER_CNTO_US_CONST_WRAPPER(a)  BITTER_CNTO_US_CONST_WRAPPER_(a)
#define BITTER_CNTO_US_CAST_WRAPPER(a)   BITTER_CAST(unsigned int, a)
#define BITTER_CNTO_US_INDEX_WRAPPER(a)  a
#define BITTER_CNTO_US_IMPL(resref, value)                                                       \
    BITTER_CNTO_PORTABLE_KERNEL(resref, value, BITTER_USHRT_WIDTH, BITTER_CNTO_US_CONST_WRAPPER, \
        BITTER_CNTO_US_CAST_WRAPPER, BITTER_CNTO_US_INDEX_WRAPPER, unsigned short, unsigned int, unsigned int)

#define BITTER_CNTO_UI_CONST_WRAPPER_(a) a##u
#define BITTER_CNTO_UI_CONST_WRAPPER(a)  BITTER_CNTO_UI_CONST_WRAPPER_(a)
#define BITTER_CNTO_UI_CAST_WRAPPER(a)   a
#define BITTER_CNTO_UI_INDEX_WRAPPER(a)  a
#define BITTER_CNTO_UI_IMPL(resref, value)                                                      \
    BITTER_CNTO_PORTABLE_KERNEL(resref, value, BITTER_UINT_WIDTH, BITTER_CNTO_UI_CONST_WRAPPER, \
        BITTER_CNTO_UI_CAST_WRAPPER, BITTER_CNTO_UI_INDEX_WRAPPER, unsigned int, unsigned int, unsigned int)

#define BITTER_CNTO_UL_CONST_WRAPPER_(a) a##ul
#define BITTER_CNTO_UL_CONST_WRAPPER(a)  BITTER_CNTO_UL_CONST_WRAPPER_(a)
#define BITTER_CNTO_UL_CAST_WRAPPER(a)   a
#define BITTER_CNTO_UL_INDEX_WRAPPER(a)  a
#define BITTER_CNTO_UL_IMPL(resref, value)                                                       \
    BITTER_CNTO_PORTABLE_KERNEL(resref, value, BITTER_ULONG_WIDTH, BITTER_CNTO_UL_CONST_WRAPPER, \
        BITTER_CNTO_UL_CAST_WRAPPER, BITTER_CNTO_UL_INDEX_WRAPPER, unsigned long, unsigned long, unsigned long)

#if BITTER_HAS_UNSIGNED_LONG_LONG
#    define BITTER_CNTO_ULL_CONST_WRAPPER_(a) a##ull
#    define BITTER_CNTO_ULL_CONST_WRAPPER(a)  BITTER_CNTO_ULL_CONST_WRAPPER_(a)
#    define BITTER_CNTO_ULL_CAST_WRAPPER(a)   a
#    define BITTER_CNTO_ULL_INDEX_WRAPPER(a)  a
#    define BITTER_CNTO_ULL_IMPL(resref, value)                                                                  \
        BITTER_CNTO_PORTABLE_KERNEL(res, value, BITTER_ULLONG_WIDTH, BITTER_CNTO_ULL_CONST_WRAPPER,              \
            BITTER_CNTO_ULL_CAST_WRAPPER, BITTER_CNTO_ULL_INDEX_WRAPPER, unsigned long long, unsigned long long, \
            unsigned long long)
#endif

#endif /* BITTER_KERNELS_KCNTO_H */
