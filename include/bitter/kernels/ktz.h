/*
 * Copyright (c) 2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_KERNELS_KTZ_H
#define BITTER_KERNELS_KTZ_H

#include <bitter/detail/types.h>
#include <bitter/detail/util.h>
#include <bitter/detail/widths.h>

#define BITTER_TZ_UC_IMPL(resref, value)                                    \
    do {                                                                    \
        unsigned int idx;                                                   \
        (resref) = BITTER_UCHAR_WIDTH;                                      \
        for (idx = 0u; idx < BITTER_UCHAR_WIDTH; idx++) {                   \
            if ((BITTER_CAST(unsigned int, (value)) & (1u << idx)) != 0u) { \
                (resref) = idx;                                             \
                break;                                                      \
            }                                                               \
        }                                                                   \
    } while (0)

#define BITTER_TZ_US_IMPL(resref, value)                                    \
    do {                                                                    \
        unsigned int idx;                                                   \
        (resref) = BITTER_USHRT_WIDTH;                                      \
        for (idx = 0u; idx < BITTER_USHRT_WIDTH; idx++) {                   \
            if ((BITTER_CAST(unsigned int, (value)) & (1u << idx)) != 0u) { \
                (resref) = idx;                                             \
                break;                                                      \
            }                                                               \
        }                                                                   \
    } while (0)

#define BITTER_TZ_UI_IMPL(resref, value)                 \
    do {                                                 \
        unsigned int idx;                                \
        (resref) = BITTER_UINT_WIDTH;                    \
        for (idx = 0u; idx < BITTER_UINT_WIDTH; idx++) { \
            if (((value) & (1u << idx)) != 0u) {         \
                (resref) = idx;                          \
                break;                                   \
            }                                            \
        }                                                \
    } while (0)

#define BITTER_TZ_UL_IMPL(resref, value)                  \
    do {                                                  \
        unsigned int idx;                                 \
        (resref) = BITTER_ULONG_WIDTH;                    \
        for (idx = 0u; idx < BITTER_ULONG_WIDTH; idx++) { \
            if (((value) & (1ul << idx)) != 0ul) {        \
                (resref) = idx;                           \
                break;                                    \
            }                                             \
        }                                                 \
    } while (0)

#if BITTER_HAS_UNSIGNED_LONG_LONG

#    define BITTER_TZ_ULL_IMPL(resref, value)                  \
        do {                                                   \
            unsigned int idx;                                  \
            (resref) = BITTER_ULLONG_WIDTH;                    \
            for (idx = 0u; idx < BITTER_ULLONG_WIDTH; idx++) { \
                if (((value) & (1ull << idx)) != 0ull) {       \
                    (resref) = idx;                            \
                    break;                                     \
                }                                              \
            }                                                  \
        } while (0)

#endif

#endif /* BITTER_KERNELS_KTZ_H */
