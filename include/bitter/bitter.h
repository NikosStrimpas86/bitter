/*
 * Copyright (c) 2023-2024 Nikolaos Strimpas
 *
 * This software is provided ‘as-is’, without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *      1. The origin of this software must not be misrepresented; you must not
 *      claim that you wrote the original software. If you use this software
 *      in a product, an acknowledgment in the product documentation would be
 *      appreciated but is not required.
 *
 *      2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *      3. This notice may not be removed or altered from any source
 *      distribution.
 */

#ifndef BITTER_INCLUDE_BITTER_H
#define BITTER_INCLUDE_BITTER_H

#include <bitter/detail/types.h>
#include <bitter/detail/util.h>
#include <bitter/detail/widths.h>
#include <bitter/impl/endian.h>

#define BITTER_ENDIAN_LITTLE BITTER_ENDIAN_LITTLE_IMPL
#define BITTER_ENDIAN_BIG    BITTER_ENDIAN_BIG_IMPL
#define BITTER_ENDIAN_NATIVE BITTER_ENDIAN_NATIVE_IMPL

enum bitter_endian {
    bitter_endian_little = BITTER_ENDIAN_LITTLE,
    bitter_endian_big = BITTER_ENDIAN_BIG,
    bitter_endian_native = BITTER_ENDIAN_NATIVE
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_leading_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_leading_ones_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_trailing_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_trailing_ones_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_ones_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_ones_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_trailing_ones_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_trailing_ones_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_zero_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_first_leading_one_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_zero_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_zero_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_zero_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_zero_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_zero_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_one_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_one_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_one_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_one_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_first_trailing_one_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_count_zeros_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_count_ones_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_ones_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_count_ones_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL bitter_bool bitter_has_single_bit_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned int bitter_bit_width_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_bit_width_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned int bitter_bit_width_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned char bitter_bit_floor_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned short bitter_bit_floor_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_bit_floor_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned long bitter_bit_floor_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned long long bitter_bit_floor_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

BITTER_UNSEQ_DECL unsigned char bitter_bit_ceil_uc(unsigned char value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned short bitter_bit_ceil_us(unsigned short value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned int bitter_bit_ceil_ui(unsigned int value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
BITTER_UNSEQ_DECL unsigned long bitter_bit_ceil_ul(unsigned long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#if BITTER_HAS_UNSIGNED_LONG_LONG
BITTER_UNSEQ_DECL unsigned long long bitter_bit_ceil_ull(unsigned long long value) BITTER_NOEXCEPT BITTER_UNSEQ_FNT;
#endif /* BITTER_HAS_UNSIGNED_LONG_LONG */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BITTER_INCLUDE_BITTER_H */

#ifdef BITTER_IMPLEMENTATION

/* clang-format off */
#include <bitter/impl/lz.h>
#include <bitter/impl/lo.h>
#include <bitter/impl/tz.h>
#include <bitter/impl/to.h>
#include <bitter/impl/flo.h>
#include <bitter/impl/flz.h>
#include <bitter/impl/fto.h>
#include <bitter/impl/ftz.h>
#include <bitter/impl/cnto.h>
#include <bitter/impl/cntz.h>
#include <bitter/impl/bitwidth.h>
#include <bitter/impl/bitceil.h>
#include <bitter/impl/bitfloor.h>
#include <bitter/impl/hsb.h>
/* clang-format on */

#endif /* BITTER_IMPLEMENTATION */
